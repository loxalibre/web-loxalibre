var express = require("express");

var facade = require("../facades/project.facade");
var utils = require('./utils');

var router = express.Router();

router.get("/test", facade.test);
router.get("/", utils.is_Admin, facade.list);
router.get("/delete/:id", utils.is_Admin, facade.delete);
router.get("/down/:id", utils.is_Admin, facade.down);
router.get("/update/:id", utils.is_Admin, facade.show_Update);

router.get("/new", utils.is_Admin, facade.show_New);

router.post("/new", utils.is_Admin, facade.new);
router.post("/update/:id", utils.is_Admin, facade.update);

module.exports = router;
