var express = require('express');
var passport = require('passport');

var facade = require('../facades/user.facade');
var utils = require('./utils');

var router = express.Router();

/* GET */

router.get('/', utils.is_Admin, facade.list);
router.get('/down/:id', utils.is_Admin, facade.down);
router.get('/delete/:id', utils.is_Admin, facade.delete);
router.get('/update/:id', utils.is_Admin, facade.show_update_admin);
router.get('/profile/:id', utils.is_Authenticated, facade.profile);

router.get('/verify', passport.authenticate('localapikey', {successRedirect: '/users/profile', failureRedirect: '/users/register' }));

router.get('/login', function(req, res, next) {
  res.render("admin/user/login", { title: "Acceder - LoxaLibre", message: req.flash('message') });
});

router.get('/profile', utils.is_Authenticated, function(req, res, next) {
  res.render("admin/user/home", { title: "Perfil - LoxaLibre", user_auth: req.user, user: req.user, message: req.flash('message') });
});


router.get('/register', function(req, res, next) {
  res.render("admin/user/new", { title: "Registrarse - LoxaLibre", message: req.flash('message') });
});

router.get('/update', utils.is_Authenticated, function(req, res, next) {
  res.render("admin/user/edit", { title: "Modificar Datos - LoxaLibre", user_auth: req.user, user: req.user, message: req.flash('message') });
});

router.get('/logout', function(req, res, next){
  req.logout();
  req.session.destroy(function (err) {
    res.locals.user = undefined;
    res.redirect('/');
  });
});

/* POST */

router.post('/search', utils.is_Authenticated, facade.search);
router.post('/update', utils.is_Authenticated, facade.update);
router.post('/update/:id', utils.is_Authenticated, facade.update);
router.post('/save', passport.authenticate('local-signup', {session: false, successRedirect: '/users/login', failureRedirect: '/users/register'}));
router.post('/auth', passport.authenticate('local-signin',  {successRedirect: '/users/profile', failureRedirect: '/users/login' }));

module.exports = router;
