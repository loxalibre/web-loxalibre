var express = require("express");

const Binnacle = require('../models/binnacle.model');

var router = express.Router();

/* GET home page. */
router.get("/validate/:ci", function (req, res, next) {
  var cedula = req.params.ci.trim();
  var cedula_number = [];
  var iniciales = cedula.substring(0, 4);
  var iniciales_2 = cedula.substring(0, 2);
  var iniciales_3 = cedula.substring(0, 3);
  var apariciones_2 = 0;
  var apariciones_3 = 0;
  for (var i = 0; i < cedula.length; i++) {
    cedula_number.push(Number(cedula.charAt(i)));
    if (i > 1 && ((i+1) % 2) == 0 && cedula.substring((i-1), (i+1)) == iniciales_2) {
      apariciones_2++;
    }
    if (i > 2 && ((i+1) % 3) == 0 && cedula.substring((i-2), (i+1)) == iniciales_3) {
      apariciones_3++;
    }
  }
  if (iniciales != cedula.substring(4, 8) && apariciones_2 < 4  && apariciones_3 < 2) {
    var digito_region = Number(cedula.substring(0, 2));
    if(digito_region > 0 && digito_region <= 24){
      var ultimo_digito = Number(cedula.substring(9, 10));
      var tercer_digito = Number(cedula.charAt(2));
      if (tercer_digito >= 0 && tercer_digito < 6) {
        var imp = 0;
        var par = 0;
        var d = cedula_number;
        for (var i = 0; i < d.length; i += 2) {
          d[i] = ((d[i] * 2) > 9) ? ((d[i] * 2) - 9) : (d[i] * 2);
          imp += d[i];
        }
        for (var i = 1; i < (d.length - 1); i += 2) {
          par += d[i];
        }
        var suma = imp + par;
        var d10 = Number(String(suma + 10).substring(0, 1) + "0") - suma;
        d10 = (d10 == 10) ? 0 : d10;
        if (d10 == d[9]) {
          res.send("Cedula correcta");
        }else {
          res.send("Cedula Incorrecta");
        }
      }else{
        res.send("Cédula invalida, tercer digito incorrecto");
      }
    }else{
      res.send("Código de región invalido");
    }
  }else{
    res.send("La cédula es Incorrecta");
  }
});

router.get("/", function (req, res, next) {
  let novedades = [{title: "Docker y Privacidad", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea ...", image: "/images/corousel/maxresdefault_live.jpg", number: "10", month: "SEP"}, {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", number: "05", month:"OCT"}];
  let articulos = [{title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea ...", image: "/images/corousel/maxresdefault_live_menf.jpg", number: "20", month: "AGO"}, {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", number: "05", month:"OCT"}];
  res.render("landing", { title: "LoxaLibre", novedades: novedades, articulos: articulos, message: req.flash('message') });
});

router.get("/actividades", function (req, res, next) {
  let charlas = [
    {title: "LoxaLibre", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/drupal-mas-que-un-cms-3-728.jpg", date: "14 de Julio del 2020"},
    {title: "LoxaLibre", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/drupal-mas-que-un-cms-3-728.jpg", date: "14 de Julio del 2020"},
    {title: "LoxaLibre", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/drupal-mas-que-un-cms-3-728.jpg", date: "14 de Julio del 2020"},
    {title: "LoxaLibre", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/drupal-mas-que-un-cms-3-728.jpg", date: "14 de Julio del 2020"},
    {title: "LoxaLibre", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/drupal-mas-que-un-cms-3-728.jpg", date: "14 de Julio del 2020"},
    {title: "LoxaLibre", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/drupal-mas-que-un-cms-3-728.jpg", date: "14 de Julio del 2020"}
  ];
  let eventos =[
    {title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "15 de Agosto del 2020"},
    {title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "15 de Agosto del 2020"},
    {title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "15 de Agosto del 2020"},
    {title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "15 de Agosto del 2020"},
    {title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "15 de Agosto del 2020"},
    {title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "15 de Agosto del 2020"}
  ];
  let talleres = [
    {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "05 de Octubre del 2020"},
    {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "05 de Octubre del 2020"},
    {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "05 de Octubre del 2020"},
    {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "05 de Octubre del 2020"},
    {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "05 de Octubre del 2020"},
    {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "05 de Octubre del 2020"}
  ];
  let proyectos = [
    {title: "Docker y Privacidad", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live.jpg", date: "08 de Septiembre del 2020"},
    {title: "Docker y Privacidad", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live.jpg", date: "08 de Septiembre del 2020"},
    {title: "Docker y Privacidad", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live.jpg", date: "08 de Septiembre del 2020"},
    {title: "Docker y Privacidad", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live.jpg", date: "08 de Septiembre del 2020"},
    {title: "Docker y Privacidad", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live.jpg", date: "08 de Septiembre del 2020"},
    {title: "Docker y Privacidad", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live.jpg", date: "08 de Septiembre del 2020"},
  ];
  res.render("activity", { title: "Actividades - LoxaLibre", charlas: charlas, eventos: eventos, talleres: talleres, proyectos: proyectos, message: req.flash('message') });
});

router.get("/estafeta", function (req, res) {
  let articulos = [
    {title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "15 de Agosto del 2020"},
    {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "05 de Octubre del 2020"},
    {title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "15 de Agosto del 2020"},
    {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "05 de Octubre del 2020"},
    {title: "Desarrollo Full stack MENF", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "15 de Agosto del 2020"},
    {title: "Gina Brito", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "05 de Octubre del 2020"},
  ];
  res.render("mailbox", { title: "Estafeta - LoxaLibre", estafeta: articulos, message: req.flash('message') });
});

router.get("/read", function (req, res) {
  res.render("full_text", { title: "TITULO DEL ARTICULO - LoxaLibre", message: req.flash('message') });
});

router.get("/quienes_somos", function (req, res) {
  let timeline = [
    {id: 1, title: "Flisol", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", image: "/images/corousel/maxresdefault_live_menf.jpg", date: "21 de Marzo del 2014"},
    {id: 2, title: "Week Geek", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", image: "/images/corousel/Ej5iwwTXYAEiUEY.jpg", date: "04 de Marzo del 2014"},
    {id: 3, title: "SEBSL", text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ...", full_text: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", image: "/images/corousel/maxresdefault_live.jpg", date: "01 de Abril del 2014"},
  ];
  res.render("about", { title: "Quienes somos - LoxaLibre", timeline: timeline, message: req.flash('message') });
});

router.get("/bitacora", function (req, res) {
  if (req.user) {
    Binnacle.find({}).populate("author").sort({date_action: -1}).exec((error_find, binnacle_find) => {
      if (error_find) {
        console.error(error_find);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
        res.redirect("/");
      }else{
        res.render("binnacle", { title: "Bitácora - LoxaLibre", binnacle: binnacle_find, message: req.flash('message') });
      }
    });
  }else{
    req.flash('message', {icon: "error", msg: 'No puedes acceder sin iniciar sesión'});
    res.redirect("/");
  }
});

module.exports = router;
