var express = require("express");

var facade = require("../facades/about.facade");
var utils = require('./utils');

var router = express.Router();

router.get("/test", facade.test);
router.get("/", utils.is_Admin, facade.list);
router.get("/delete/slider/:id/:img", utils.is_Admin, facade.delete_Slider);
router.get("/delete/point/:id/:point", utils.is_Admin, facade.delete_Points);
router.get("/delete/:id", utils.is_Admin, facade.delete);
router.get("/down/slider/:id/:img", utils.is_Admin, facade.down_Slider);
router.get("/down/point/:id/:point", utils.is_Admin, facade.down_Points);
router.get("/down/:id", utils.is_Admin, facade.down);
router.get("/list/slider/:id", utils.is_Admin, facade.show_List_Slider);
router.get("/list/points/:id", utils.is_Admin, facade.show_List_Points);
router.get("/update/:id", utils.is_Admin, facade.show_Update);
router.get("/new/point/:id", utils.is_Admin, facade.show_New_Point);
router.get("/update/point/:id/:point", utils.is_Admin, facade.show_Update_Point);

router.get("/new", utils.is_Admin, (req, res) => {
  res.render("admin/about/new", { title: "About - LoxaLibre", about_find: null, message: req.flash('message') });
});

router.post("/new", utils.is_Admin, facade.new);
router.post("/update/:id", utils.is_Admin, facade.update);
router.post("/add/point/:id", utils.is_Admin, facade.add_Point);
router.post("/update/point/:id/:point", utils.is_Admin, facade.update_Point);
router.post("/add/image/:id", utils.is_Admin, facade.add_Img);


module.exports = router;
