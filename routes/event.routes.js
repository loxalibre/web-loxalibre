var express = require("express");

var facade = require("../facades/event.facade");
var { is_Admin, is_Editor } = require("./utils");
const Tag = require("../models/tag.model");

var router = express.Router();

router.get("/new", is_Admin || is_Editor, (req, res, next) => {
  Tag.find({}, (err, tag_find) => {
    if (err) {
      console.error(err);
      req.flash("message", {
        icon: "error",
        msg: "Ocurrió un error al obtener lo tags.",
      });
      res.redirect("/activities/");
    } else {
      res.render("admin/event/new", {
        title: "Actividad - LoxaLibre",
        event: null,
        tags: tag_find,
        message: req.flash("message"),
      });
    }
  });
});

router.get("/test", facade.test);
router.get("/", is_Admin || is_Editor, facade.list);
router.get("/update/:id", is_Admin || is_Editor, facade.one);
router.get("/toggle/:id", is_Admin || is_Editor, facade.toggleStatus);

router.post("/save", is_Admin || is_Editor, facade.save);

router.put("/update/:id", is_Admin || is_Editor, facade.update);
router.put("/delete", is_Admin || is_Editor, facade.delete);

module.exports = router;
