var express = require("express");

var facade = require("../facades/article.facade");
var utils = require('./utils');

const Tag = require('../models/tag.model');

var router = express.Router();

router.get("/test", facade.test);
router.get("/", utils.is_Admin, facade.list);
router.get("/delete/:id", utils.is_Admin, facade.delete);
router.get("/down/:id", utils.is_Admin, facade.down);
router.get("/update/:id", utils.is_Admin, facade.show_Update);

router.get("/new", utils.is_Admin, (req, res) => {
  Tag.find({}, (err, tag_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener lo tags.'});
      res.redirect("/article/");
    }else{
      res.render("admin/article/new", { title: "Articulo - LoxaLibre", article_find: null, tags: tag_find, message: req.flash('message') });
    }
  });
});

router.post("/new", utils.is_Admin, facade.new);
router.post("/update/:id", utils.is_Admin, facade.update);

module.exports = router;
