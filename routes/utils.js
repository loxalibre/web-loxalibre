exports.is_Authenticated = function (req, res, next) {
  if(!req.isAuthenticated()){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder.'});
    res.redirect('/');
  }else{
    next();
  }
}

exports.is_Admin = function (req, res, next) {
  if(!req.isAuthenticated() || req.user.rol != "Admin"){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder.'});
    res.redirect('/');
  }else{
    next();
  }
}

exports.is_Editor = function (req, res, next) {
  if(!req.isAuthenticated() || req.user.rol != "Admin" || req.user.rol != "Editor"){
    req.flash('message', {icon: "warning", msg: 'No puedes acceder.'});
    res.redirect('/');
  }else{
    next();
  }
}
