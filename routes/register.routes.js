var express = require("express");
var facade = require("../facades/register.facade");
var { is_Admin, is_Editor } = require("./utils");

var router = express.Router();

router.get("/new", is_Admin || is_Editor, facade.new);

router.get("/test", is_Admin || is_Editor, facade.test);
router.get("/", is_Admin || is_Editor, facade.list);
// router.get("/update/:id", is_Admin || is_Editor, facade.one);
router.get("/toggle/:id", is_Admin || is_Editor, facade.toggleStatus);

router.post("/save", is_Admin || is_Editor, facade.save);
router.post("/update/:id", is_Admin || is_Editor, facade.update);

router.put("/delete", is_Admin || is_Editor, facade.delete);

module.exports = router;
