var express = require("express");

var facade = require("../facades/setting.facade");
var { is_Admin } = require("./utils");
var router = express.Router();

router.get("/new", is_Admin, function (req, res, next) {
  res.render("admin/setting/new", {
    title: "Registrar configuración",
    user_auth: req.user,
    user: req.user,
    setting: null,
    message: req.flash("message"),
  });
});

router.get("/test", is_Admin, facade.test);
router.get("/", is_Admin, facade.list);
router.get("/update/:id", is_Admin, facade.one);
router.get("/toggle/:id", is_Admin, facade.toggleStatus);

router.post("/save", is_Admin, facade.save);

router.put("/update/:id", is_Admin, facade.update);
router.put("/delete", is_Admin, facade.delete);

module.exports = router;
