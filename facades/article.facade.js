"use strict";

var controller = require("../controllers/article.controller");

exports.test = function (req, res) {
  return controller.test(req, res);
};

exports.list = function (req, res) {
  return controller.list(req, res);
};
exports.new = function (req, res) {
  return controller.new(req, res);
};

exports.update = function (req, res) {
  return controller.update(req, res);
};

exports.show_Update = function (req, res) {
  return controller.show_Update(req, res);
};

exports.down = function (req, res) {
  return controller.down(req, res);
};

exports.delete = function (req, res) {
  return controller.delete(req, res);
};
