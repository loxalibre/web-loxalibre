'use strict'

var controller = require('../controllers/user.controller');

exports.test = function (req, res) {
    return controller.test(req, res);
};

exports.list = function (req, res) {
    return controller.list(req, res);
};

exports.profile = function (req, res) {
    return controller.profile(req, res);
};

exports.update = function (req, res) {
    return controller.update(req, res);
};

exports.show_update_admin = function (req, res) {
    return controller.show_update_admin(req, res);
};

exports.down = function (req, res) {
    return controller.down(req, res);
};

exports.delete = function (req, res) {
    return controller.delete(req, res);
};

exports.search = function (req, res) {
    return controller.search(req, res);
};
