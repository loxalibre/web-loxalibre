"use strict";

var controller = require("../controllers/register.controller");

exports.new = function (req, res) {
  return controller.new(req, res);
};

exports.test = function (req, res) {
  return controller.test(req, res);
};

exports.list = function (req, res) {
  return controller.list(req, res);
};

// exports.one = function (req, res) {
//   return controller.one(req, res);
// };

exports.save = function (req, res) {
  return controller.save(req, res);
};

exports.update = function (req, res) {
  return controller.update(req, res);
};

exports.toggleStatus = function (req, res) {
  return controller.toggleStatus(req, res);
};

exports.delete = function (req, res) {
  return controller.delete(req, res);
};
