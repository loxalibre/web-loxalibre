"use strict";

var controller = require("../controllers/about.controller");

exports.test = function (req, res) {
  return controller.test(req, res);
};

exports.list = function (req, res) {
  return controller.list(req, res);
};

exports.show_Update = function (req, res) {
  return controller.show_Update(req, res);
};

exports.show_List_Slider = function (req, res) {
  return controller.show_List_Slider(req, res);
};

exports.show_List_Points = function (req, res) {
  return controller.show_List_Points(req, res);
};

exports.new = function (req, res) {
  return controller.new(req, res);
};

exports.update = function (req, res) {
  return controller.update(req, res);
};

exports.add_Point = function (req, res) {
  return controller.add_Point(req, res);
};

exports.update_Point = function (req, res) {
  return controller.update_Point(req, res);
};

exports.show_Update_Point = function (req, res) {
  return controller.show_Update_Point(req, res);
};

exports.show_New_Point = function (req, res) {
  return controller.show_New_Point(req, res);
};

exports.add_Img = function (req, res) {
  return controller.add_Img(req, res);
};

exports.down = function (req, res) {
  return controller.down(req, res);
};

exports.down_Slider = function (req, res) {
  return controller.down_Slider(req, res);
};

exports.down_Points = function (req, res) {
  return controller.down_Points(req, res);
};

exports.delete = function (req, res) {
  return controller.delete(req, res);
};

exports.delete_Slider = function (req, res) {
  return controller.delete_Slider(req, res);
};

exports.delete_Points = function (req, res) {
  return controller.delete_Points(req, res);
};
