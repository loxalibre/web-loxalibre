var bCrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer');
const fileUpload = require('express-fileupload');
const path = require('path');
const fs = require('fs');

const User = require('../../models/user.model');

module.exports = function (passport) {

    var LocalStrategy = require('passport-local').Strategy;
    var LocalAPIKeyStrategy = require("passport-localapikey-update").Strategy;

    passport.serializeUser(function (account, done) {
        done(null, account);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        if(id){
          User.findOne({_id: id._id}, (error, user) => {
            if (error) {
              console.error(error);
              done(error, null);
            }else{
              done(null, user);
            }
          });
        } else {
            done(id.errors, null);
        }
    });
    //registro de usuarios por passport
    passport.use('local-signup', new LocalStrategy(
        {
            usernameField: 'email_account',//lo que esta como name en el input del registro
            passwordField: 'password_account',//lo que esta como name en el input del registro
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, email, password, done) {
          var letras_reg_ex = /^[ñÑáéíóúÁÉÍÓÚa-zA-Z ]{2,254}$/;
          var letras_mayusculas = "ÁÉÍÓÚABCDEFGHYJKLMNÑOPQRSTUVWXYZ";
          var letras_minusculas = "áéíóúabcdefghyjklmnñopqrstuvwxyz";
          var numeros = "0123456789";
          let tiene_numero = false;
          let tiene_mayuscula = false;
          let tiene_minuscula = false;
          if (password.length > 7) {
            for(i = 0; i < password.length; i++){
              if (numeros.indexOf(password.charAt(i), 0) != -1){
                tiene_numero = true;
              }
              if (letras_mayusculas.indexOf(password.charAt(i), 0) != -1){
                tiene_mayuscula = true;
              }
              if (letras_minusculas.indexOf(password.charAt(i), 0) != -1){
                tiene_minuscula = true;
              }
            }
          }
          if(!email && !password && !req.body.name_user && !letras_reg_ex.test(req.body.name_user) && !req.body.last_name_user && !letras_reg_ex.test(req.body.last_name_user) && !tiene_numero && !tiene_mayuscula && !tiene_minuscula){
            if (!tiene_numero || !tiene_mayuscula || !tiene_minuscula) {
              req.flash('message', {icon: "error", msg: 'La contraseña debe contener almenos 1 numero, 1 mayuscula y tener almenos 8 caracteres.'});
            }
            req.flash('message', {icon: "error", msg: 'Debe ingresar todos los campos correctamente.'});
            return done(null, false, {
              message: 'Debe Ingresar Todos los datos'
            });
          }else{
            //verificar si el email esta correcto
            var emailRegEx = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
            if(!emailRegEx.test(email)){
              req.flash('message', {icon: "error", msg: 'El correo ingresado es incorrecto.'});
              return done(null, false, {
                message: 'El Correo Ingresado es Incorrecto'
              });
            }else{
              if (req.body.password_confirm != password) {
                req.flash('message', {icon: "error", msg: 'Las claves no coinciden.'});
                return done(null, false, {
                  message: 'Las claves no coinciden.'
                });
              }else{
                var generateHash = function (password) {
                  return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
                };
                //verificar si el email no esta registrado y guardar el usuario
                User.findOne({mail: email}, function (err, user) {
                  if (err){
                    console.error(err);
                    req.flash('message', {icon: "error", msg: 'Ocurrió un error al intentar buscar el correo en la base de datos'});
                    return done(null, false, {
                      message: 'Ocurrió un error al intentar buscar el correo en la base de datos'
                    });
                  }else{
                    if(user){
                      req.flash('message', {icon: "error", msg: 'El correo ingresado ya existe.'});
                      return done(null, false, {
                        message: 'El Correo Ingresado ya existe'
                      });
                    }else{
                      var str_img = "/images/user/user_default.png";
                      let user_aux = new User({
                        name: req.body.name_user,
                        last_name: req.body.last_name_user,
                        password: generateHash(password),
                        photo: str_img,
                        mail: email,
                        photo: str_img,
                        updated_at: new Date()
                      });

                      user_aux.save(function (error, newUser) {
                        if (error) {
                          console.error(error);
                          req.flash('message', {icon: "error", msg: 'Ocurrió un error al registrar el nuevo usuario'});
                          return done(null, false, {
                            message: "Ocurrió un error al registrar el nuevo usuario"
                          });
                        }else{
                          if (false) {
                            var transporter = nodemailer.createTransport({
                              host: "smtp.gmail.com",
                              port: 465,
                              secure: true,
                              auth:{
                                type: "login",
                                user: 'paxdevsoft@gmail.com',
                                pass: 'paxDevSoft2018'
                              }
                            });
                            // Definimos el email
                            var mailOptions = {
                              from: 'LoxaLibre <loxalibre@gmail.com>',
                              to: email,
                              subject: 'Bienvenido a LoxaLibre',
                              text: "Te extendemos nuestra cordial bienvenida de parte de quienes conformamos la comunidad de software libre de Loja.\n Para terminar tu registro activa tu cuenta accediendo al siguiente link:\n " + THIS_DOMAIN + "verify/?apikey=" + newUser._id + "\n\nLoxaLibre Uniendo a Loja con software libre.\nRedes sociales:\nFacebook: https://www.facebook.com/Loxalibre\nTwiter: https://twitter.com/loxalibre\nYouTuve: https://www.youtube.com/channel/UC34WkhpWMgWw4rJ50t1m4Lg"
                            };
                            // Enviamos el email
                            transporter.sendMail(mailOptions, function(error, info){
                              if (error){
                                console.error(error);
                                req.flash("message", {icon: "warning", msg: 'Ocurrió un error al enviar el correo de verificación'});
                                return done(null, false, {
                                  message: "Ocurrió un error al enviar el correo de verificación"
                                });
                              } else {
                                console.log("Email sent");
                                req.flash('message', {icon: "success", msg: 'Gracias por crear su cuenta, Por favor verifique su correo y active su cuenta.'});
                                return done(null, newUser);
                              }
                            });
                          }else{
                            req.flash('message', {icon: "success", msg: 'Gracias por crear su cuenta.'});
                            return done(null, newUser);
                          }
                        }
                      });
                    }
                  }
                });
              }
            }
          }
        }
    ));
    //inicio de sesion
    passport.use('local-signin', new LocalStrategy(
        {
            usernameField: 'email',
            passwordField: 'pass',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        },
        function (req, email, password, done) {
          if(!email && !password){
            req.flash('message', {icon: "warning", msg: 'Debe ingresar todos los datos.'});
            return done(null, false, {
              message: 'Debe Ingresar Todos los datos'
            });
          }else{
            var emailRegEx = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
            if(!emailRegEx.test(email)){
              req.flash('message', {icon: "error", msg: 'El correo Ingresado es incorrecto.'});
              return done(null, false, {
                message: 'El Correo Ingresado es Incorrecto'
              });
            }else{
              var isValidPassword = function (userpass, password) {
                return bCrypt.compareSync(password, userpass);
              }

              User.findOne({mail: email}, function (err, user) {
                if (err){
                  console.error(err);
                  req.flash('message', {icon: "error", msg: 'Ocurrió un error al intentar buscar el correo en la base de datos'});
                  return done(null, false, {
                    message: 'Ocurrió un error al intentar buscar el correo en la base de datos'
                  });
                }else{
                  if (!user) {
                    req.flash('message', {icon: "error", msg: 'El correo no existe.'});
                    return done(null, false, {message: 'Correo no existe'});
                  }else{
                    if(!user.status){
                      req.flash('message', {icon: "error", msg: 'Su cuenta esta desactivada, revise su email.'});
                      return done(null, false, {message: 'Su cuenta esta Desactivada.'});
                    }else{
                      if (!isValidPassword(user.password, password)) {
                        req.flash('message', {icon: "error", msg: 'Clave incorrecta.'});
                        return done(null, false, {message: 'Clave incorrecta.'});
                      }else{
                        req.flash('message', {icon: "success", msg: 'Bienvenido ' + user.name + " " + user.last_name});
                        var userinfo = user;
                        return done(null, userinfo);
                      }
                    }
                  }
                }
              });
            }
          }
        }
    ));
    //verificar correo
    passport.use(new LocalAPIKeyStrategy(
      function(apikey, done) {
        User.findById(apikey, function (err, user) {
          if (err) {
            console.error(err);
            return done(null, false, {mensaje: 'error al obtener el usuario'});
          }else {
            if (!user) {
              return done(null, false, {mensaje: 'No se reconoce la el usuario'});
            }else{
              user.active_user = true;
              user.save(function (error, updatedRegistry) {
                if (error) {
                  console.log("Error de Activación: " + error);
                  return done(null, false, {mensaje: 'No se pudo actualizar el estado del usuario'});
                }else{
                  console.log("cuenta activada");
                  var userinfo = user;
                  return done(null, userinfo);
                }
              });
            }
          }
        });
      }
    ));
}
