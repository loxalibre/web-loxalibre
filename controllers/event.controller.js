"use strict";

const Event = require("../models/event.model");
const Binnacle = require("../models/binnacle.model");
const Tag = require("../models/tag.model");

const fs = require("fs");
const { types_image } = require("./utils");
/***** VIEWS *****/

/* test */
exports.test = function (req, res) {
  res.send("Greetings from the Event controller!");
};
/**/
exports.list = async function (req, res) {
  try {
    // queries
    const { page, rowsPerPage, orderBy, direction } = req.query;
    const res_per_page = parseFloat(rowsPerPage) || 10;
    const current_page = parseFloat(page) || 1;

    const listEvent = await Event.find()
      .sort({ [orderBy || "createdAt"]: direction || "desc" })
      .skip(res_per_page * current_page - res_per_page)
      .limit(res_per_page);
    const total = await Event.countDocuments();
    res.render("admin/event/list", {
      title: "Eventos - LoxaLibre",
      event: listEvent,
      total: total,
      message: req.flash("message"),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al buscar.",
    });
  }
};

/***** CONTROLLERS *****/
exports.one = async function (req, res) {
  try {
    const { id } = req.params;
    const event = await Event.findById(id).populate("tags");
    if (!event) {
      req.flash("message", {
        icon: "error",
        msg: "El evento no existe",
      });
      res.redirect("/event");
    }

    const tags = await Tag.find();

    res.render("admin/event/new", {
      title: "Evento - LoxaLibre",
      event,
      tags,
      message: req.flash("message"),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al buscar.",
    });
    res.redirect("/event");
  }
};

exports.save = async function (req, res) {
  try {
    let data = JSON.parse(req.body.data);
    data.responsible = req.user._id;

    let tags = data.tags.split(",");
    var tags_id = [];
    if (Array.isArray(tags) && tags.length > 0) {
      for (var i = 0; i < tags.length; i++) {
        let name = tags[i].trim().toUpperCase();
        const tag_find = await Tag.findOne({ name: name });
        if (tag_find) {
          tags_id.push(tag_find._id);
        } else {
          let new_tag = new Tag({ name: name });
          await new_tag.save();
          tags_id.push(new_tag._id);
        }
      }
    }
    data.tags = tags_id;

    const newEvent = new Event(data);

    if (req.files.art) {
      let path_to_folder = __dirname.replace(
        "controllers",
        `public/images/event/${newEvent._id}/`
      );

      fs.mkdirSync(path_to_folder, { recursive: true }, (err) => {
        if (err) throw err;
      });

      var writeStream = fs.createWriteStream(path_to_folder + ".gitignore");
      writeStream.write("*");
      writeStream.close();
      let artFile = req.files.art;

      artFile.mv(
        path_to_folder + newEvent._id + "." + types_image[artFile.mimetype],
        (errorofs) => {
          errorofs ? console.error(errorofs) : console.log("foto subida");
        }
      );

      newEvent.art =
        `/images/event/${newEvent._id}/` +
        newEvent._id +
        "." +
        types_image[artFile.mimetype];
    }

    if (data.organizer) {
      for (const itemOrganizer of data.organizer) {
        let path_to_folder = __dirname.replace(
          "controllers",
          `public/images/event/${newEvent._id}/organizer/`
        );

        fs.mkdirSync(path_to_folder, { recursive: true }, (err) => {
          if (err) throw err;
        });

        var writeStream = fs.createWriteStream(path_to_folder + ".gitignore");
        writeStream.write("*");
        writeStream.close();

        let logoPhoto = req.files[`logo-${itemOrganizer.id}`];

        logoPhoto.mv(
          path_to_folder +
            newEvent.organizer[itemOrganizer.id]._id +
            "." +
            types_image[logoPhoto.mimetype],
          (errorofs) => {
            errorofs ? console.error(errorofs) : console.log("foto subida");
          }
        );

        newEvent.organizer[itemOrganizer.id].logo =
          `/images/event/${newEvent._id}/organizer/` +
          newEvent.organizer[itemOrganizer.id]._id +
          "." +
          types_image[logoPhoto.mimetype];
      }
    }

    await newEvent.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha creado el evento: " +
        newEvent.title,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", { icon: "success", msg: "Creado con éxito." });
    res.json({ redirect: "/event" });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al guardar.",
    });
    res.json({ redirect: "/event/new" });
  }
};

exports.update = async function (req, res) {
  try {
    console.log("\n\nBODY UPDATE", JSON.parse(req.body.data));
    console.log("\n\nFILES UPDATE", req.files);

    let data = JSON.parse(req.body.data);
    let event = await Event.findById(req.params.id);
    event.title = data.title;
    event.lead = data.lead;
    event.description = data.description;
    event.registration_start = data.registration_start;
    event.registration_end = data.registration_end;
    event.means_places = data.means_places;
    event.organizer = data.organizer;

    let tags = data.tags.split(",");
    var tags_id = [];
    if (Array.isArray(tags) && tags.length > 0) {
      for (var i = 0; i < tags.length; i++) {
        let name = tags[i].trim().toUpperCase();
        const tag_find = await Tag.findOne({ name: name });
        if (tag_find) {
          tags_id.push(tag_find._id);
        } else {
          let new_tag = new Tag({ name: name });
          await new_tag.save();
          tags_id.push(new_tag._id);
        }
      }
    }
    event.tags = tags_id;

    if (req.files && req.files.art) {
      let path_to_folder = __dirname.replace(
        "controllers",
        `public/images/event/${req.params.id}/`
      );

      let artFile = req.files.art;

      artFile.mv(
        path_to_folder + req.params.id + "." + types_image[artFile.mimetype],
        (errorofs) => {
          errorofs ? console.error(errorofs) : console.log("foto subida");
        }
      );
    }

    if (data.organizer) {
      for (const itemLogo of data.organizer) {
        let path_to_folder = __dirname.replace(
          "controllers",
          `public/images/event/${event._id}/organizer/`
        );

        let organizerPhoto = req.files[`logo-${itemLogo.id}`];

        if (organizerPhoto) {
          let organizerId = event.organizer[itemLogo.id]
            ? event.organizer[itemLogo.id]._id
            : mongoose.Types.ObjectId();

          organizerPhoto.mv(
            path_to_folder +
              organizerId +
              "." +
              types_image[organizerPhoto.mimetype],
            (errorofs) => {
              errorofs ? console.error(errorofs) : console.log("foto subida");
            }
          );

          event.organizer[itemLogo.id].logo =
            `/images/event/${event._id}/organizer/` +
            organizerId +
            "." +
            types_image[organizerPhoto.mimetype];

          event.organizer[itemLogo.id]._id = organizerId;
        }
      }
    }

    await event.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha modificado el evento: " +
        event.title,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: "Modificado con éxito",
    });
    res.json({ redirect: "/event" });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al modificar.",
    });
    res.json({ redirect: `/event/update/${req.params.id}` });
  }
};

exports.toggleStatus = async function (req, res) {
  try {
    let event = await Event.findById(req.params.id);
    event.status = !event.status;
    await event.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha cambiado el estado del evento: " +
        event.title,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: event.status ? "Activado con éxito" : "Suspendido con éxito.",
    });
    res.redirect(`/event`);
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al suspender.",
    });
    res.redirect(`/event`);
  }
};

exports.delete = async function (req, res) {
  try {
   let event = await Event.findByIdAndDelete(req.params.id);
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha eliminado el evento: " +
        event.title,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: "Eliminado con éxito",
    });
    res.redirect(`/event`);
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al eliminar.",
    });
    res.redirect(`/event`);
  }
};
