'use strict'

var fs = require("fs");

const About = require('../models/about.model');
const Binnacle = require('../models/binnacle.model');

/***** VIEWS *****/

/* test */
exports.test = function (req, res) {
  res.send('Greetings from the About controller!');
};

exports.list = function (req, res) {
  About.find({}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/");
    }else{
      res.render("admin/about/list", { title: "About - LoxaLibre", abouts_find: about_find, message: req.flash('message') });
    }
  });
};

exports.show_Update = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/");
    }else{
      res.render("admin/about/new", { title: "About - LoxaLibre", about_find: about_find, message: req.flash('message') });
    }
  });
};

exports.show_New_Point = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/");
    }else{
      res.render("admin/about/new_point", { title: "About - LoxaLibre", point_find: null, about_find: about_find, message: req.flash('message') });
    }
  });
};

exports.show_Update_Point = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/");
    }else{
      let point_find = null;
      if (about_find && about_find.points && about_find.points.length > 0) {
        for (var i = 0; i < about_find.points.length; i++) {
          let item = about_find.points[i];
          if (String(item._id) == String(req.params.point)) {
            point_find = item;
          }
        }
      }
      res.render("admin/about/new_point", { title: "About - LoxaLibre", point_find: point_find, about_find: about_find, message: req.flash('message') });
    }
  });
};

exports.show_List_Slider = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/");
    }else{
      res.render("admin/about/list_images", { title: "About - LoxaLibre", about_find: about_find, message: req.flash('message') });
    }
  });
};

exports.show_List_Points = function (req, res) {
  About.findOne({_id: req.params.id}).populate("points.author").exec((err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/");
    }else{
      res.render("admin/about/list_points", { title: "About - LoxaLibre", about_find: about_find, message: req.flash('message') });
    }
  });
};

/***** CONTROLLERS *****/

exports.new = function (req, res) {
  if (req.body.title && req.body.introduction) {
    var path_to_forder = __dirname.replace("controllers", "public/images/slider/about/");
    fs.mkdirSync(path_to_forder, { recursive: true }, (err) => {
      if (err) throw err;
      if (err) {
        console.error(err);
      }
    });
    var writeStream = fs.createWriteStream(path_to_forder + ".gitignore");
    writeStream.write("*");
    writeStream.close();
    let slider = [];
    if(req.files && req.files.photo_user && req.files.photo_user.length > 0){
      for (var i = 0; i < req.files.photo_user.length; i++) {
        let EDFile = req.files.photo_user[i];
        EDFile.mv(path_to_forder + EDFile.name, errorofs => {
          if(errorofs){
            console.error(errorofs);
          }else{
            console.log("foto subida");
          }
        });
        slider.push({img: "/images/slider/about/" + EDFile.name});
      }
    }else if (req.files && req.files.photo_user) {
      let EDFile = req.files.photo_user;
      EDFile.mv(path_to_forder + EDFile.name, errorofs => {
        if(errorofs){
          console.error(errorofs);
        }else{
          console.log("foto subida");
        }
      });
      slider.push({img: "/images/slider/about/" + EDFile.name});
    }
    new About({
      title: req.body.title,
      introduction: req.body.introduction,
      image_slider: slider,
      updated_at: new Date()
    }).save((err, new_about) => {
      if (err) {
        console.error(err);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el about.'});
        res.redirect("/about/new");
      }else{
        new Binnacle({
          date_action: new Date(),
          description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha creado un nuevo About.",
          author: req.user
        }).save();
        req.flash('message', {icon: "success", msg: 'About creado con éxito.'});
        res.redirect("/about");
      }
    });
  }else{
    req.flash('message', {icon: "warning", msg: 'No se han enviado los datos obigatorios.'});
    res.redirect("/about/new");
  }
};

exports.update = function (req, res) {
  if (req.body.title && req.body.introduction) {
    About.findOne({_id: req.params.id}, (error, about_find) => {
      if (error) {
        console.error(error);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el about.'});
        res.redirect("/about/update/" + req.params.id);
      }else{
        if (about_find) {
          about_find.title = req.body.title;
          about_find.introduction = req.body.introduction;
          about_find.updated_at = new Date();
          about_find.save((err, new_about) => {
            if (err) {
              console.error(err);
              req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el about.'});
              res.redirect("/about/update/" + req.params.id);
            }else{
              new Binnacle({
                date_action: new Date(),
                description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha editado el About: " + about_find.title,
                author: req.user
              }).save();
              req.flash('message', {icon: "success", msg: 'About editado con éxito.'});
              res.redirect("/about");
            }
          });
        }else{
          req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
          res.redirect("/about/update/" + req.params.id);
        }
      }
    });
  }else{
    req.flash('message', {icon: "warning", msg: 'No se han enviado los datos obigatorios.'});
    res.redirect("/about/update/" + req.params.id);
  }
};

exports.add_Point = function (req, res) {
  if (req.body.title && req.body.lead && req.body.description && req.body.description_json) {
    About.findOne({_id: req.params.id}, (error, about_find) => {
      if (error) {
        console.error(error);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el about.'});
        res.redirect("/about/list/points/" + req.params.id);
      }else{
        if (about_find) {
          req.body.author = req.user;
          req.body.date_updated = new Date();
          req.body.description_json = req.body.description_json.replace(/\r\n/g, "");
          let points = (about_find.points && about_find.points.length > 0)? about_find.points : [];
          points.push(req.body);
          about_find.points = points;
          about_find.save((err, new_about) => {
            if (err) {
              console.error(err);
              req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el about.'});
              res.redirect("/about/list/points/" + req.params.id);
            }else{
              new Binnacle({
                date_action: new Date(),
                description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha agregado el Point: " + req.body.title + " en el About: " + about_find.title,
                author: req.user
              }).save();
              req.flash('message', {icon: "success", msg: 'About agregado con éxito.'});
              res.redirect("/about/list/points/" + req.params.id);
            }
          });
        }else{
          req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
          res.redirect("/about/list/points/" + req.params.id);
        }
      }
    });
  }else{
    req.flash('message', {icon: "warning", msg: 'No se han enviado los datos obigatorios.'});
    res.redirect("/about/list/points/" + req.params.id);
  }
};

exports.update_Point = function (req, res) {
  if (req.body.title && req.body.lead && req.body.description && req.body.description_json) {
    About.findOne({_id: req.params.id}, (error, about_find) => {
      if (error) {
        console.error(error);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el about.'});
        res.redirect("/about/list/points/" + req.params.id);
      }else{
        if (about_find) {
          req.body.description_json = req.body.description_json.replace(/\r\n/g, "");
          var new_list_points = [];
          if (about_find.points && about_find.points.length > 0) {
            for(let i = 0; i < about_find.points.length; i++){
              let item = about_find.points[i];
              if (String(item._id) == String(req.params.point)) {
                item.title = req.body.title;
                item.lead = req.body.lead;
                item.description = req.body.description;
                item.description_json = req.body.description_json;
                item.date_updated = new Date();
              }
              new_list_points.push(item);
            }
          }
          about_find.points = new_list_points;
          about_find.save((err, new_about) => {
            if (err) {
              console.error(err);
              req.flash('message', {icon: "error", msg: 'Ocurrió un error al editar el punto del about.'});
              res.redirect("/about/list/points/" + req.params.id);
            }else{
              new Binnacle({
                date_action: new Date(),
                description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha editado el Point: " + req.body.title + " en el About: " + about_find.title,
                author: req.user
              }).save();
              req.flash('message', {icon: "success", msg: 'Punto del About editado con éxito.'});
              res.redirect("/about/list/points/" + req.params.id);
            }
          });
        }else{
          req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
          res.redirect("/about/list/points/" + req.params.id);
        }
      }
    });
  }else{
    req.flash('message', {icon: "warning", msg: 'No se han enviado los datos obigatorios.'});
    res.redirect("/about/update/" + req.params.id);
  }
};

exports.add_Img = function (req, res) {
  if (req.files && req.files.photo_user) {
    var path_to_forder = __dirname.replace("controllers", "public/images/slider/about/");
    fs.mkdirSync(path_to_forder, { recursive: true }, (err) => {
      if (err) throw err;
      if (err) {
        console.error(err);
      }
    });
    var writeStream = fs.createWriteStream(path_to_forder + ".gitignore");
    writeStream.write("*");
    writeStream.close();
    About.findOne({_id: req.params.id}, (error, about_find) => {
      if (error) {
        console.error(error);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el about.'});
        res.redirect("/about/list/slider/" + req.params.id);
      }else{
        if (about_find) {
          let slider = (about_find.image_slider && about_find.image_slider.length > 0)? about_find.image_slider : [];
          if(req.files.photo_user.length > 0){
            for (var i = 0; i < req.files.photo_user.length; i++) {
              let EDFile = req.files.photo_user[i];
              EDFile.mv(path_to_forder + EDFile.name, errorofs => {
                if(errorofs){
                  console.error(errorofs);
                }else{
                  console.log("foto subida");
                }
              });
              slider.push({img: "/images/slider/about/" + EDFile.name});
            }
          }else{
            let EDFile = req.files.photo_user;
            EDFile.mv(path_to_forder + EDFile.name, errorofs => {
              if(errorofs){
                console.error(errorofs);
              }else{
                console.log("foto subida");
              }
            });
            slider.push({img: "/images/slider/about/" + EDFile.name});
          }
          about_find.image_slider = slider;
          about_find.save((err, new_about) => {
            if (err) {
              console.error(err);
              req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el about.'});
              res.redirect("/about/list/slider/" + req.params.id);
            }else{
              new Binnacle({
                date_action: new Date(),
                description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha agregado imagenes en el About: " + about_find.title,
                author: req.user
              }).save();
              req.flash('message', {icon: "success", msg: 'About editado con éxito.'});
              res.redirect("/about/list/slider/" + req.params.id);
            }
          });
        }else{
          req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
          res.redirect("/about/list/slider/" + req.params.id);
        }
      }
    });
  }else{
    req.flash('message', {icon: "warning", msg: 'No se ha enviado ninguna imagen.'});
    res.redirect("/about/list/slider/" + req.params.id);
  }
};

exports.down = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/about");
    }else{
      if (about_find) {
        about_find.status = (about_find.status)? false : true;
        about_find.save((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al cambiar el estado del about.'});
            res.redirect("/about");
          }else{
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha cambiado el estado del About: \"" + about_find.title + "\", a " + about_find.status,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El estado del about ha sido cambiando con éxito.'});
            res.redirect("/about");
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
        res.redirect("/about");
      }
    }
  });
};

exports.down_Slider = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/about/update/slider/" + req.params.id);
    }else{
      if (about_find) {
        let flag = false;
        var new_list_images = [];
        if (about_find.image_slider && about_find.image_slider.length > 0) {
          for(let i = 0; i < about_find.image_slider.length; i++){
            let item = about_find.image_slider[i];
            if (String(item._id) == String(req.params.img)) {
              item.isVisible = (item.isVisible)? false : true;
              flag = item.isVisible;
            }
            new_list_images.push(item);
          }
        }
        about_find.image_slider = new_list_images;
        about_find.save((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al guardar la nueva lista de imagenes del slider del about.'});
            res.redirect("/about/update/slider/" + req.params.id);
          }else{
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha puesto el estado de visivilidad en: " + flag + " de una imagen del slider en el About: " + about_find.title,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El listado de imagenes del slider del about ha sido actualizado.'});
            res.redirect("/about/update/slider/" + req.params.id);
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
        res.redirect("/about/update/slider/" + req.params.id);
      }
    }
  });
};

exports.down_Points = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/about/update/points/" + req.params.id);
    }else{
      if (about_find) {
        let flag = false;
        var new_list_points = [];
        if (about_find.points && about_find.points.length > 0) {
          for(let i = 0; i < about_find.points.length; i++){
            let item = about_find.points[i];
            if (String(item._id) == String(req.params.point)) {
              item.isVisible = (item.isVisible)? false : true;
              flag = item.isVisible;
            }
            new_list_points.push(item);
          }
        }
        about_find.points = new_list_points;
        about_find.save((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al guardar la nueva lista de Puntos del about.'});
            res.redirect("/about/update/points/" + req.params.id);
          }else{
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha puesto el estado de visivilidad en: " + flag + " de un punto del timeline en el About: " + about_find.title,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El listado de Puntos del about ha sido actualizado.'});
            res.redirect("/about/update/points/" + req.params.id);
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
        res.redirect("/about/update/points/" + req.params.id);
      }
    }
  });
};

exports.delete = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/about");
    }else{
      if (about_find) {
        about_find.delete((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al eliminar el about.'});
            res.redirect("/about");
          }else{
            if (about_find.image_slider && about_find.image_slider.length > 0) {
              for(let i = 0; i < about_find.image_slider.length; i++){
                let item = about_find.image_slider[i];
                fs.stat(__dirname.replace("controllers", "public") + item.img, function(err) {
                  if (!err) {
                    console.log('file or directory existe');
                    fs.unlinkSync(__dirname.replace("controllers", "public") + item.img);
                  }else if (err.code === 'ENOENT') {
                    console.log('file or directory does not exist');
                  }
                });
              }
            }
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha eliminado el About: " + about_find.title,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El about ha sido eliminado con éxito.'});
            res.redirect("/about");
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
        res.redirect("/about");
      }
    }
  });
};

exports.delete_Slider = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/about/update/slider/" + req.params.id);
    }else{
      if (about_find) {
        var new_list_images = [];
        if (about_find.image_slider && about_find.image_slider.length > 0) {
          for(let i = 0; i < about_find.image_slider.length; i++){
            let item = about_find.image_slider[i];
            if (String(item._id) == String(req.params.img)) {
              fs.stat(__dirname.replace("controllers", "public") + item.img, function(err) {
                if (!err) {
                  console.log('file or directory existe');
                  fs.unlinkSync(__dirname.replace("controllers", "public") + item.img);
                }else if (err.code === 'ENOENT') {
                  console.log('file or directory does not exist');
                }
              });
            }else{
              new_list_images.push(item);
            }
          }
        }
        about_find.image_slider = new_list_images;
        about_find.save((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al guardar la nueva lista de imagenes del slider del about.'});
            res.redirect("/about/update/slider/" + req.params.id);
          }else{
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha eliminado una imagen del slider del About: " + about_find.title,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El listado de imagenes del slider del about ha sido actualizado.'});
            res.redirect("/about/update/slider/" + req.params.id);
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
        res.redirect("/about/update/slider/" + req.params.id);
      }
    }
  });
};

exports.delete_Points = function (req, res) {
  About.findOne({_id: req.params.id}, (err, about_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/about/update/points/" + req.params.id);
    }else{
      if (about_find) {
        var new_list_points = [];
        if (about_find.points && about_find.points.length > 0) {
          for(let i = 0; i < about_find.points.length; i++){
            let item = about_find.points[i];
            if (String(item._id) != String(req.params.point)) {
              new_list_points.push(item);
            }
          }
        }
        about_find.points = new_list_points;
        about_find.save((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al guardar la nueva lista de Puntos del about.'});
            res.redirect("/about/update/points/" + req.params.id);
          }else{
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha eliminado una punto del timeline del About: " + about_find.title,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El listado de Puntos del about ha sido actualizado.'});
            res.redirect("/about/update/points/" + req.params.id);
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el about.'});
        res.redirect("/about/update/points/" + req.params.id);
      }
    }
  });
};
