'use strict'

var fs = require("fs");

const Project = require('../models/project.model');
const User = require('../models/user.model');
const Tag = require('../models/tag.model');
const Binnacle = require('../models/binnacle.model');

const types_image={
  'image/png': 'png',
  'image/bmp': 'bmp',
  'image/cis-cod': 'cod',
  'image/gif': 'gif',
  'image/ief': 'ief',
  'image/jpeg': 'jpeg',
  'image/jpg': 'jpg',
  'image/pipeg': 'jfif',
  'image/svg+xml': 'svg',
  'image/tiff': 'tif',
  'image/x-cmu-raster': 'ras',
  'image/x-cmx': 'cmx',
  'image/x-icon': 'ico',
  'image/x-portable-anymap': 'pnm',
  'image/x-portable-bitmap': 'pbm',
  'image/x-portable-graymap': 'pgm',
  'image/x-portable-pixmap': 'ppm',
  'image/x-rgb': 'rgb',
  'image/x-xbitmap': 'xbm',
  'image/x-xpixmap': 'xpm',
  'image/x-xwindowdump': 'xwd'
};

/***** VIEWS *****/

/* test */
exports.test = function (req, res) {
  res.send('Greetings from the Project controller!');
};

exports.list = function (req, res) {
  Project.find({}).populate(["pm", "team"]).exec((err, project_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/");
    }else{
      res.render("admin/project/list", { title: "Proyectos - LoxaLibre", projects_find: project_find, message: req.flash('message') });
    }
  });
};

exports.show_New = function (req, res) {
  Tag.find({}, (err, tag_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener lo tags.'});
      res.redirect("/project");
    }else{
      res.render("admin/project/new", { title: "Proyectos - LoxaLibre", tags: tag_find, project_find: null, message: req.flash('message') });
    }
  });
};

exports.show_Update = function (req, res) {
  Project.findOne({_id: req.params.id}).populate(["team", "tags"]).exec((err, project_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/project");
    }else{
      Tag.find({}, (err, tag_find) => {
        if (err) {
          console.error(err);
          req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener lo tags.'});
          res.redirect("/project");
        }else{
          res.render("admin/project/new", { title: "Proyectos - LoxaLibre", tags: tag_find, project_find: project_find, message: req.flash('message') });
        }
      });
    }
  });
};

/***** CONTROLLERS *****/

exports.new = async function (req, res) {
  let tags = req.body.tags.split(",");
  var tags_id = [];
  if (Array.isArray(tags) && tags.length > 0) {
    for (var i = 0; i < tags.length; i++) {
      let name = tags[i].trim().toUpperCase();
      const tag_find = await Tag.findOne({name:name});
      if (tag_find) {
        tags_id.push(tag_find._id);
      }else{
        let new_tag = new Tag({name: name});
        await new_tag.save();
        tags_id.push(new_tag._id);
      }
    }
  }
  if (req.body.name && req.body.isPM && req.body.team && req.body.lead && req.body.date_start && req.body.duration && req.body.description && req.body.description_json && req.files && req.files.art) {
    var path_to_forder = __dirname.replace("controllers", "public/images/project/");
    fs.mkdirSync(path_to_forder, { recursive: true }, (err) => {
      if (err) throw err;
      if (err) {
        console.error(err);
      }
    });
    var writeStream = fs.createWriteStream(path_to_forder + ".gitignore");
    writeStream.write("*");
    writeStream.close();
    let EDFile = req.files.art;
    let team = [];
    if (req.body.team && Array.isArray(req.body.team)) {
      team = req.body.team;
    }else{
      team.push(req.body.team);
    }
    let new_project = new Project({
      name: req.body.name,
      lead: req.body.lead,
      date_start: new Date(req.body.date_start.split("-")[0], req.body.date_start.split("-")[1], req.body.date_start.split("-")[2], 0, 0, 0),
      duration: req.body.duration,
      description: req.body.description,
      pm: req.body.isPM,
      team: team,
      description_json: req.body.description_json,
      tags: tags_id,
      updated_at: new Date()
    });
    EDFile.mv(path_to_forder + new_project._id + "." + types_image[EDFile.mimetype], errorofs => {
      if(errorofs){
        console.error(errorofs);
      }else{
        console.log("foto subida");
      }
    });
    new_project.art = "/images/project/" + new_project._id + "." + types_image[EDFile.mimetype];
    new_project.save((err, new_project) => {
      if (err) {
        console.error(err);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el proyecto.'});
        res.redirect("/project/new");
      }else{
        new Binnacle({
          date_action: new Date(),
          description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha creado el Project: " + new_project.title,
          author: req.user
        }).save();
        req.flash('message', {icon: "success", msg: 'Proyecto creado con éxito.'});
        res.redirect("/project");
      }
    });
  }else{
    console.error(req.body);
    req.flash('message', {icon: "warning", msg: 'No se han enviado los datos obigatorios.'});
    res.redirect("/project/new");
  }
};

exports.update = function (req, res) {
  if (req.body.name && req.body.isPM && req.body.team && req.body.lead && req.body.date_start && req.body.duration && req.body.lead && req.body.description && req.body.description_json) {
    Project.findOne({_id: req.params.id}, async(error, project_find) => {
      if (error) {
        console.error(error);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el proyecto.'});
        res.redirect("/project/update/" + req.params.id);
      }else{
        if (project_find) {
          let tags = req.body.tags.split(",");
          var tags_id = [];
          if (Array.isArray(tags) && tags.length > 0) {
            for (var i = 0; i < tags.length; i++) {
              let name = tags[i].trim().toUpperCase();
              const tag_find = await Tag.findOne({name:name});
              if (tag_find) {
                tags_id.push(tag_find._id);
              }else{
                let new_tag = new Tag({name: name});
                await new_tag.save();
                tags_id.push(new_tag._id);
              }
            }
          }
          let team = [];
          if (req.body.team && Array.isArray(req.body.team)) {
            team = req.body.team;
          }else{
            team.push(req.body.team);
          }
          project_find.name = req.body.name;
          project_find.lead = req.body.lead;
          project_find.pm = req.body.isPM;
          project_find.team = team;
          project_find.date_start = new Date(req.body.date_start.split("-")[0], req.body.date_start.split("-")[1], req.body.date_start.split("-")[2], 0, 0, 0);
          project_find.duration = req.body.duration;
          project_find.description = req.body.description;
          project_find.description_json = req.body.description_json;
          project_find.updated_at = new Date();
          project_find.tags = tags_id;
          if(req.files && req.files.art){
            fs.stat(__dirname.replace("controllers", "public") + project_find.art, function(err) {
              if (!err) {
                console.log('file or directory existe');
                fs.unlinkSync(__dirname.replace("controllers", "public") + project_find.art);
              }else if (err.code === 'ENOENT') {
                console.log('file or directory does not exist');
              }
            });
            let EDFile = req.files.art;
            EDFile.mv(__dirname.replace("controllers", "public/images/project/") + project_find._id + "." + types_image[EDFile.mimetype], errorofs => {
              if(errorofs){
                console.error(errorofs);
              }else{
                console.log("foto subida");
              }
            });
            project_find.art = "/images/project/" + project_find._id + "." + types_image[EDFile.mimetype];
          }
          project_find.save((err, new_project) => {
            if (err) {
              console.error(err);
              req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el proyecto.'});
              res.redirect("/project/update/" + req.params.id);
            }else{
              new Binnacle({
                date_action: new Date(),
                description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha editado el Project: " + project_find.title,
                author: req.user
              }).save();
              req.flash('message', {icon: "success", msg: 'Proyecto editado con éxito.'});
              res.redirect("/project");
            }
          });
        }else{
          req.flash('message', {icon: "warning", msg: 'No se reconoce el proyecto.'});
          res.redirect("/project/update/" + req.params.id);
        }
      }
    });
  }else{
    req.flash('message', {icon: "warning", msg: 'No se han enviado los datos obigatorios.'});
    res.redirect("/project/update/" + req.params.id);
  }
};

exports.down = function (req, res) {
  Project.findOne({_id: req.params.id}, (err, project_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/project");
    }else{
      if (project_find) {
        project_find.status = (project_find.status)? false : true;
        project_find.save((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al cambiar el estado del proyecto.'});
            res.redirect("/project");
          }else{
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha cambiado el estado del Project: \"" + project_find.title + "\", a " + project_find.status,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El estado del proyecto ha sido cambiando con éxito.'});
            res.redirect("/project");
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el proyecto.'});
        res.redirect("/project");
      }
    }
  });
};

exports.delete = function (req, res) {
  Project.findOne({_id: req.params.id}, (err, project_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/project");
    }else{
      if (project_find) {
        project_find.delete((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al eliminar el project.'});
            res.redirect("/project");
          }else{
            fs.stat(__dirname.replace("controllers", "public") + project_find.art, function(err) {
              if (!err) {
                console.log('file or directory existe');
                fs.unlinkSync(__dirname.replace("controllers", "public") + project_find.art);
              }else if (err.code === 'ENOENT') {
                console.log('file or directory does not exist');
              }
            });
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha eliminado el Project: " + project_find.title,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El proyecto ha sido eliminado con éxito.'});
            res.redirect("/project");
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el proyecto.'});
        res.redirect("/project");
      }
    }
  });
};
