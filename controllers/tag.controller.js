"use strict";

const Tag = require("../models/tag.model");
const Binnacle = require("../models/binnacle.model");

/***** VIEWS *****/

/* test */
exports.test = function (req, res) {
  res.send("Greetings from the Tag controller!");
};
/**/
exports.list = async function (req, res) {
  try {
    // queries
    const { page, rowsPerPage, orderBy, direction } = req.query;
    const res_per_page = parseFloat(rowsPerPage) || 10;
    const current_page = parseFloat(page) || 1;

    const listTag = await Tag.find()
      .sort({ [orderBy || "createdAt"]: direction || "desc" })
      .skip(res_per_page * current_page - res_per_page)
      .limit(res_per_page);
    const total = await Tag.countDocuments();
    res.render("admin/tag/list", {
      title: "Etiquetas - LoxaLibre",
      tag: listTag,
      total: total,
      message: req.flash("message"),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al buscar.",
    });
  }
};

/***** CONTROLLERS *****/
exports.one = async function (req, res) {
  try {
    const { id } = req.params;
    const tag = await Tag.findById(id);
    if (!tag) {
      req.flash("message", {
        icon: "error",
        msg: "La etiqueta no existe",
      });
      res.redirect("/tag");
    }

    res.render("admin/tag/new", {
      title: "Etiqueta - LoxaLibre",
      tag,
      message: req.flash("message"),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al buscar.",
    });
    res.redirect("/tag");
  }
};

exports.save = async function (req, res) {
  try {
    const newTag = new Tag(req.body);
    await newTag.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +": " + req.user.name +" " +req.user.last_name +" ha creado la etiqueta: " +
        newTag.name,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "error",
      msg: "Creado con éxito",
    });
    res.redirect("/tag");
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al guardar.",
    });
    res.redirect("/tag/new");
  }
};

exports.update = async function (req, res) {
  try {
    let tag = await Tag.findByIdAndUpdate(req.params.id, req.body);
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha modificado la etiqueta: " +
        tag.name,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: "Modificado con éxito",
    });
    res.redirect("/tag");
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al modificar.",
    });
    res.redirect("/tag");
  }
};

exports.toggleStatus = async function (req, res) {
  try {
    let tag = await Tag.findById(req.params.id);
    tag.status = !tag.status;
    await tag.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha cambiado el estado de la etiqueta: " +
        tag.name,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: tag.status ? "Activado con éxito" : "Suspendido con éxito.",
    });
    res.redirect(`/tag`);
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al suspender.",
    });
    res.redirect(`/tag`);
  }
};

exports.delete = async function (req, res) {
  try {
    let tag = await Tag.findByIdAndDelete(req.params.id);
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha eliminado la etiqueta: " +
        tag.name,
      author: data.author,
    });
    await binnacle.save();
    res.render("/tag", {
      title: "Etiquetas - LoxaLibre",
      message: req.flash("message", {
        icon: "success",
        msg: "Eliminado con éxito",
      }),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al eliminar.",
    });
  }
};
