'use strict'

var fs = require("fs");

const Article = require('../models/article.model');
const Tag = require('../models/tag.model');
const Binnacle = require('../models/binnacle.model');

const types_image={
  'image/png': 'png',
  'image/bmp': 'bmp',
  'image/cis-cod': 'cod',
  'image/gif': 'gif',
  'image/ief': 'ief',
  'image/jpeg': 'jpeg',
  'image/jpg': 'jpg',
  'image/pipeg': 'jfif',
  'image/svg+xml': 'svg',
  'image/tiff': 'tif',
  'image/x-cmu-raster': 'ras',
  'image/x-cmx': 'cmx',
  'image/x-icon': 'ico',
  'image/x-portable-anymap': 'pnm',
  'image/x-portable-bitmap': 'pbm',
  'image/x-portable-graymap': 'pgm',
  'image/x-portable-pixmap': 'ppm',
  'image/x-rgb': 'rgb',
  'image/x-xbitmap': 'xbm',
  'image/x-xpixmap': 'xpm',
  'image/x-xwindowdump': 'xwd'
};

/***** VIEWS *****/

/* test */
exports.test = function (req, res) {
  res.send('Greetings from the Article controller!');
};

exports.list = function (req, res) {
  Article.find({}, (err, article_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/");
    }else{
      res.render("admin/article/list", { title: "Articulo - LoxaLibre", articles_find: article_find, message: req.flash('message') });
    }
  });
};

exports.show_Update = function (req, res) {
  Article.findOne({_id: req.params.id}).populate("tags").exec((err, article_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/article");
    }else{
      Tag.find({}, (err, tag_find) => {
        if (err) {
          console.error(err);
          req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener lo tags.'});
          res.redirect("/article");
        }else{
          res.render("admin/article/new", { title: "Articulo - LoxaLibre", tags: tag_find, article_find: article_find, message: req.flash('message') });
        }
      });
    }
  });
};

/***** CONTROLLERS *****/

exports.new = async function (req, res) {
  let tags = req.body.tags.split(",");
  var tags_id = [];
  if (Array.isArray(tags) && tags.length > 0) {
    for (var i = 0; i < tags.length; i++) {
      let name = tags[i].trim().toUpperCase();
      const tag_find = await Tag.findOne({name:name});
      if (tag_find) {
        tags_id.push(tag_find._id);
      }else{
        let new_tag = new Tag({name: name});
        await new_tag.save();
        tags_id.push(new_tag._id);
      }
    }
  }
  if (req.body.title && req.body.lead && req.body.description && req.body.description_json && req.files && req.files.art) {
    var path_to_forder = __dirname.replace("controllers", "public/images/article/");
    fs.mkdirSync(path_to_forder, { recursive: true }, (err) => {
      if (err) throw err;
      if (err) {
        console.error(err);
      }
    });
    var writeStream = fs.createWriteStream(path_to_forder + ".gitignore");
    writeStream.write("*");
    writeStream.close();
    let EDFile = req.files.art;
    let new_article = new Article({
      title: req.body.title,
      lead: req.body.lead,
      description: req.body.description,
      description_json: req.body.description_json,
      tags: tags_id,
      updated_at: new Date()
    });
    EDFile.mv(path_to_forder + new_article._id + "." + types_image[EDFile.mimetype], errorofs => {
      if(errorofs){
        console.error(errorofs);
      }else{
        console.log("foto subida");
      }
    });
    new_article.art = "/images/article/" + new_article._id + "." + types_image[EDFile.mimetype];
    new_article.save((err, new_article) => {
      if (err) {
        console.error(err);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el article.'});
        res.redirect("/article/new");
      }else{
        new Binnacle({
          date_action: new Date(),
          description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha creado el Article: " + new_article.title,
          author: req.user
        }).save();
        req.flash('message', {icon: "success", msg: 'Article creado con éxito.'});
        res.redirect("/article");
      }
    });
  }else{
    console.error(req.body);
    req.flash('message', {icon: "warning", msg: 'No se han enviado los datos obigatorios.'});
    res.redirect("/article/new");
  }
};

exports.update = function (req, res) {
  if (req.body.title && req.body.lead && req.body.description && req.body.description_json) {
    Article.findOne({_id: req.params.id}, async(error, article_find) => {
      if (error) {
        console.error(error);
        req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el article.'});
        res.redirect("/article/update/" + req.params.id);
      }else{
        if (article_find) {
          let tags = req.body.tags.split(",");
          var tags_id = [];
          if (Array.isArray(tags) && tags.length > 0) {
            for (var i = 0; i < tags.length; i++) {
              let name = tags[i].trim().toUpperCase();
              const tag_find = await Tag.findOne({name:name});
              if (tag_find) {
                tags_id.push(tag_find._id);
              }else{
                let new_tag = new Tag({name: name});
                await new_tag.save();
                tags_id.push(new_tag._id);
              }
            }
          }
          article_find.title = req.body.title;
          article_find.lead = req.body.lead;
          article_find.description = req.body.description;
          article_find.description_json = req.body.description_json;
          article_find.updated_at = new Date();
          article_find.tags = tags_id;
          if(req.files && req.files.art){
            fs.stat(__dirname.replace("controllers", "public") + article_find.art, function(err) {
              if (!err) {
                console.log('file or directory existe');
                fs.unlinkSync(__dirname.replace("controllers", "public") + article_find.art);
              }else if (err.code === 'ENOENT') {
                console.log('file or directory does not exist');
              }
            });
            let EDFile = req.files.art;
            EDFile.mv(__dirname.replace("controllers", "public/images/article/") + article_find._id + "." + types_image[EDFile.mimetype], errorofs => {
              if(errorofs){
                console.error(errorofs);
              }else{
                console.log("foto subida");
              }
            });
            article_find.art = "/images/article/" + article_find._id + "." + types_image[EDFile.mimetype];
          }
          article_find.save((err, new_article) => {
            if (err) {
              console.error(err);
              req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener el article.'});
              res.redirect("/article/update/" + req.params.id);
            }else{
              new Binnacle({
                date_action: new Date(),
                description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha editado el Article: " + article_find.title,
                author: req.user
              }).save();
              req.flash('message', {icon: "success", msg: 'Article editado con éxito.'});
              res.redirect("/article");
            }
          });
        }else{
          req.flash('message', {icon: "warning", msg: 'No se reconoce el article.'});
          res.redirect("/article/update/" + req.params.id);
        }
      }
    });
  }else{
    req.flash('message', {icon: "warning", msg: 'No se han enviado los datos obigatorios.'});
    res.redirect("/article/update/" + req.params.id);
  }
};

exports.down = function (req, res) {
  Article.findOne({_id: req.params.id}, (err, article_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/article");
    }else{
      if (article_find) {
        article_find.status = (article_find.status)? false : true;
        article_find.save((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al cambiar el estado del article.'});
            res.redirect("/article");
          }else{
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha cambiado el estado del Article: \"" + article_find.title + "\", a " + article_find.status,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El estado del article ha sido cambiando con éxito.'});
            res.redirect("/article");
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el article.'});
        res.redirect("/article");
      }
    }
  });
};

exports.delete = function (req, res) {
  Article.findOne({_id: req.params.id}, (err, article_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los registros.'});
      res.redirect("/article");
    }else{
      if (article_find) {
        article_find.delete((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al eliminar el article.'});
            res.redirect("/article");
          }else{
            fs.stat(__dirname.replace("controllers", "public") + article_find.art, function(err) {
              if (!err) {
                console.log('file or directory existe');
                fs.unlinkSync(__dirname.replace("controllers", "public") + article_find.art);
              }else if (err.code === 'ENOENT') {
                console.log('file or directory does not exist');
              }
            });
            new Binnacle({
              date_action: new Date(),
              description: "El " + req.user.rol + ": " + req.user.name + " " + req.user.last_name + " ha eliminado el Article: " + article_find.title,
              author: req.user
            }).save();
            req.flash('message', {icon: "success", msg: 'El article ha sido eliminado con éxito.'});
            res.redirect("/article");
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el article.'});
        res.redirect("/article");
      }
    }
  });
};
