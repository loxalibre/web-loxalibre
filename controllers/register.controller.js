"use strict";
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const Register = require("../models/register.model");
const Binnacle = require("../models/binnacle.model");

/***** VIEWS *****/
var schemaCountry = new Schema({
  code: String,
  country: String
}, {
  collection: 'country'
});
var country = mongoose.model('country', schemaCountry);
mongoose.connect('mongodb://localhost:27017/loxalibre');

exports.new = async function(req, res){
  await country.find({}, (err, countries)=>{
    if (err) {
      req.flash("message", {
        icon: "error",
        msg: "Ocurrió un error al obtener lo datos.",
      });
      res.redirect("/users/profile/");
    } else {
      res.render("register/new", {
        title: "Registrar a actividad",
        user_auth: req.user,
        user: req.user,
        country: countries,
        message: req.flash("message"),
      });
    }
  })
}
/* test */
exports.test = function (req, res) {
  res.send("Greetings from the Register controller!");
};

exports.list = async function (req, res) {
  try {
    // queries
    const { page, rowsPerPage, orderBy, direction } = req.query;
    const res_per_page = parseFloat(rowsPerPage) || 10;
    const current_page = parseFloat(page) || 1;

    const listRegister = await Register.find()
      .sort({ [orderBy || "createdAt"]: direction || "desc" })
      .skip(res_per_page * current_page - res_per_page)
      .limit(res_per_page);
    const total = await Register.countDocuments();
    res.render("register", {
      title: "Registros - LoxaLibre",
      register: listRegister,
      total: total,
      message: req.flash("message"),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al buscar.",
    });
    res.redirect("/register");
  }
};

exports.save = async function (req, res) {
  try {
    const newRegister = new Register(req.body);
    await newRegister.save();
    // const binnacle = new Binnacle({
    //   date_action: new Date(),
    //   description:
    //     "El " +
    //     req.user.rol +": " + req.user.name +" " +req.user.last_name +" ha creado la registro: " +
    //     newRegister.name,
    //   author: data.author,
    // });
    // await binnacle.save();
    req.flash("message", {
      icon: "error",
      msg: "Creado con éxito",
    });
    res.redirect("/register");
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al guardar.",
    });
    res.redirect("/register/new");
  }
};
exports.update = async function (req, res) {
  try {
    let register = await Register.findByIdAndUpdate(req.params.id, req.body);
    //   const binnacle = new Binnacle({
    //     date_action: new Date(),
    //     description:
    //       "El " +
    //       req.user.rol +
    //       ": " +
    //       req.user.name +
    //       " " +
    //       req.user.last_name +
    //       " ha modificado la registro: " +
    //       register.name,
    //     author: data.author,
    //   });
    //   await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: "Modificado con éxito",
    });
    res.redirect("/register");
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al modificar.",
    });
    res.redirect("/register");
  }
};

exports.toggleStatus = async function (req, res) {
  try {
    let register = await Register.findById(req.params.id);
    register.status = !register.status;
    await register.save();
    //   const binnacle = new Binnacle({
    //     date_action: new Date(),
    //     description:
    //       "El " +
    //       req.user.rol +
    //       ": " +
    //       req.user.name +
    //       " " +
    //       req.user.last_name +
    //       " ha cambiado el estado de la registro: " +
    //       register.name,
    //     author: data.author,
    //   });
    //   await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: register.status ? "Activado con éxito" : "Suspendido con éxito.",
    });
    res.redirect(`/register`);
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al suspender.",
    });
    res.redirect(`/register`);
  }
};

exports.delete = async function (req, res) {
  try {
    let register = await Register.findByIdAndDelete(req.params.id);
    //   const binnacle = new Binnacle({
    //     date_action: new Date(),
    //     description:
    //       "El " +
    //       req.user.rol +
    //       ": " +
    //       req.user.name +
    //       " " +
    //       req.user.last_name +
    //       " ha eliminado la registro: " +
    //       register.name,
    //     author: data.author,
    //   });
    await binnacle.save();
    res.render("register", {
      title: "Registro - LoxaLibre",
      message: req.flash("message", {
        icon: "success",
        msg: "Eliminado con éxito",
      }),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al eliminar.",
    });
  }
};
