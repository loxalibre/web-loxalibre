'use strict'

var fs = require("fs");

const User = require('../models/user.model');

/***** VIEWS *****/

/* test */
exports.test = function (req, res) {
  res.send('Greetings from the User controller!');
};

exports.list = function (req, res) {
  User.find({}, (err, users_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los usuarios.'});
      res.redirect("/");
    }else{
      res.render("admin/user/list", { title: "Usuarios - LoxaLibre", users: users_find, message: req.flash('message') });
    }
  });
};

exports.profile = function (req, res) {
  User.findOne({_id: req.params.id}, (err, user_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los usuarios.'});
      res.redirect("/");
    }else{
      res.locals.user = req.user;
      res.render("admin/user/home", { title: user_find.name + " " + user_find.last_name + " - LoxaLibre", user_auth: req.user, user: user_find, message: req.flash('message') });
    }
  });
};

exports.show_update_admin = function (req, res) {
  User.findOne({_id: req.params.id}, (err, user_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los usuarios.'});
      res.redirect("/");
    }else{
      res.render("admin/user/edit", { title: user_find.name + " " + user_find.last_name + " - LoxaLibre", user_auth: req.user, user: user_find, message: req.flash('message') });
    }
  });
};

/***** CONTROLLERS *****/

exports.update = function (req, res) {
  var id_user = (req.params.id)? req.params.id : req.user._id;
  var redirect_user = (req.params.id)? "/" + req.params.id : "";
  var email = req.body.email_account
  var password = req.body.password_account
  var letras_reg_ex = /^[ñÑáéíóúÁÉÍÓÚa-zA-Z ]{2,254}$/;
  var letras_mayusculas = "ÁÉÍÓÚABCDEFGHYJKLMNÑOPQRSTUVWXYZ";
  var letras_minusculas = "áéíóúabcdefghyjklmnñopqrstuvwxyz";
  var numeros = "0123456789";
  let tiene_numero = false;
  let tiene_mayuscula = false;
  let tiene_minuscula = false;
  if (password && password != "") {
    if (password.length > 7) {
      for(i = 0; i < password.length; i++){
        if (numeros.indexOf(password.charAt(i), 0) != -1){
          tiene_numero = true;
        }
        if (letras_mayusculas.indexOf(password.charAt(i), 0) != -1){
          tiene_mayuscula = true;
        }
        if (letras_minusculas.indexOf(password.charAt(i), 0) != -1){
          tiene_minuscula = true;
        }
      }
    }
    if (!tiene_numero || !tiene_mayuscula || !tiene_minuscula) {
      req.flash('message', {icon: "error", msg: 'La contraseña debe contener almenos 1 numero, 1 mayuscula y tener almenos 8 caracteres.'});
    }
  }
  if(!email && !password && !req.body.name_user && !letras_reg_ex.test(req.body.name_user) && !req.body.last_name_user && !letras_reg_ex.test(req.body.last_name_user)){
    req.flash('message', {icon: "error", msg: 'Debe ingresar todos los campos correctamente.'});
    res.redirect("/users/update" + redirect_user);
  }else{
    //verificar si el email esta correcto
    var emailRegEx = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
    if(!emailRegEx.test(email)){
      req.flash('message', {icon: "error", msg: 'El correo ingresado es incorrecto.'});
      res.redirect("/users/update" + redirect_user);
    }else{
      if (req.body.password_confirm != password) {
        req.flash('message', {icon: "error", msg: 'Las claves no coinciden.'});
        res.redirect("/users/update" + redirect_user);
      }else{
        var generateHash = function (password) {
          return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
        };
        User.findOne({_id: id_user}, function (err, user) {
          if (err){
            console.error(err);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al intentar buscar el correo en la base de datos'});
            res.redirect("/users/update" + redirect_user);
          }else{
            if(!user){
              req.flash('message', {icon: "error", msg: 'El usuario no se reconoce.'});
              res.redirect("/users/update" + redirect_user);
            }else{
              var str_img = "/images/user/user_default.png";
              let phone_user = [];
              if (req.body.phones) {
                if (Array.isArray(req.body.phones.split(","))) {
                  phone_user = req.body.phones.split(",");
                }else{
                  phone_user.push(req.body.phones);
                }
              }
              user.phones = phone_user;
              user.name = req.body.name_user;
              user.last_name = req.body.last_name_user;
              user.biography = req.body.biography;
              user.state = req.body.state;
              user.country = req.body.country;
              user.province = req.body.province;
              user.city = req.body.city;
              user.address = req.body.address;
              user.photo = str_img;
              user.mail = email;
              user.updated_at = new Date();
              user.show_city = (req.body.show_city && req.body.show_city == "on")? true : false;
              user.show_province = (req.body.show_province && req.body.show_province == "on")? true : false;
              user.show_country = (req.body.show_country && req.body.show_country == "on")? true : false;
              user.show_mail = (req.body.show_mail && req.body.show_mail == "on")? true : false;
              user.show_phones = (req.body.show_phones && req.body.show_phones == "on")? true : false;
              user.show_address = (req.body.show_address && req.body.show_address == "on")? true : false;

              if (password && password != "") {
                user.password = generateHash(password);
              }

              const types_image={
                'image/png': 'png',
                'image/bmp': 'bmp',
                'image/cis-cod': 'cod',
                'image/gif': 'gif',
                'image/ief': 'ief',
                'image/jpeg': 'jpeg',
                'image/jpg': 'jpg',
                'image/pipeg': 'jfif',
                'image/svg+xml': 'svg',
                'image/tiff': 'tif',
                'image/x-cmu-raster': 'ras',
                'image/x-cmx': 'cmx',
                'image/x-icon': 'ico',
                'image/x-portable-anymap': 'pnm',
                'image/x-portable-bitmap': 'pbm',
                'image/x-portable-graymap': 'pgm',
                'image/x-portable-pixmap': 'ppm',
                'image/x-rgb': 'rgb',
                'image/x-xbitmap': 'xbm',
                'image/x-xpixmap': 'xpm',
                'image/x-xwindowdump': 'xwd'
              };
              if(req.files && req.files.photo_user){
                let EDFile = req.files.photo_user;
                EDFile.mv(__dirname.replace("controllers", "public/images/user/") + user._id + "." + types_image[EDFile.mimetype], errorofs => {
                  if(errorofs){
                    console.error(errorofs);
                  }else{
                    console.log("foto subida");
                  }
                });
                str_img = "/images/user/" + user._id + "." + types_image[EDFile.mimetype];
                if (user.photo != "/images/user/user_default.png") {
                  fs.stat(__dirname.replace("controllers", "public") + user.photo, function(err) {
                    if (!err) {
                      console.log('file or directory existe');
                      fs.unlinkSync(__dirname.replace("controllers", "public") + user.photo);
                    }else if (err.code === 'ENOENT') {
                      console.log('file or directory does not exist');
                    }
                  });
                }
              }
              user.photo = str_img;
              user.save(function (error, newUser) {
                if (error) {
                  console.error(error);
                  req.flash('message', {icon: "error", msg: 'Ocurrió un error al registrar el nuevo usuario'});
                  res.redirect("/users/update" + redirect_user);
                }else{
                  req.flash('message', {icon: "success", msg: 'Sus datos fueron modificados con éxito.'});
                  res.redirect("/users/update" + redirect_user);
                }
              });
            }
          }
        });
      }
    }
  }
};

exports.down = function (req, res) {
  User.findOne({_id: req.params.id}, (err, user_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los usuarios.'});
      res.redirect("/users");
    }else{
      if (user_find) {
        user_find.status = (user_find.status)? false : true;
        user_find.save((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al cambiar el estado del usuario.'});
            res.redirect("/users");
          }else{
            req.flash('message', {icon: "success", msg: 'El estado del usuario ha sido cambiando con éxito.'});
            res.redirect("/users");
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el usuario.'});
        res.redirect("/users");
      }
    }
  });
};

exports.delete = function (req, res) {
  User.findOne({_id: req.params.id}, (err, user_find) => {
    if (err) {
      console.error(err);
      req.flash('message', {icon: "error", msg: 'Ocurrió un error al obtener los usuarios.'});
      res.redirect("/users");
    }else{
      if (user_find) {
        user_find.delete((error) => {
          if (error) {
            console.error(error);
            req.flash('message', {icon: "error", msg: 'Ocurrió un error al eliminar el usuario.'});
            res.redirect("/users");
          }else{
            if (user_find.photo != "/images/user/user_default.png") {
              fs.stat(__dirname.replace("controllers", "public") + user_find.photo, function(err) {
                if (!err) {
                  console.log('file or directory existe');
                  fs.unlinkSync(__dirname.replace("controllers", "public") + user_find.photo);
                }else if (err.code === 'ENOENT') {
                  console.log('file or directory does not exist');
                }
              });
            }
            req.flash('message', {icon: "success", msg: 'El usuario ha sido eliminado con éxito.'});
            res.redirect("/users");
          }
        });
      }else{
        req.flash('message', {icon: "warning", msg: 'No se reconoce el usuario.'});
        res.redirect("/users");
      }
    }
  });
};

exports.search = function (req, res) {
  User.find({}, async (err, users_find) => {
    if (err) {
      console.error(err);
      res.json({
        code: "E001",
        status: "error",
        statusstr: "Ocurrió un error al obtener los registros de Usuarios",
        data: err
      });
    }else{
      if (users_find && users_find.length > 0) {
        var list_users = await users_find.filter(item => item.name.includes(req.body.search) || item.last_name.includes(req.body.search) || item.mail.includes(req.body.search));
        res.json({
          code: "U001",
          status: "success",
          statusstr: "Busqueda exitosa",
          data: list_users
        });
      }else {
        res.json({
          code: "E002",
          status: "warning",
          statusstr: "La lista de Usuarios esta vacia.",
          data: users_find
        });
      }
    }
  });
};
