"use strict";

const Setting = require("../models/setting.model");
const Binnacle = require("../models/binnacle.model");

const fs = require("fs");
const mongoose = require("mongoose");
const { types_image } = require("./utils");

/***** VIEWS *****/

/* test */
exports.test = function (req, res) {
  res.send("Greetings from the Setting controller!");
};
/**/
exports.list = async function (req, res) {
  try {
    // queries
    const { page, rowsPerPage, orderBy, direction } = req.query;
    const res_per_page = parseFloat(rowsPerPage) || 10;
    const current_page = parseFloat(page) || 1;

    const listSetting = await Setting.find()
      .sort({ [orderBy || "createdAt"]: direction || "desc" })
      .skip(res_per_page * current_page - res_per_page)
      .limit(res_per_page);
    const total = await Setting.countDocuments();
    res.render("admin/setting/list", {
      title: "Configuraciones - LoxaLibre",
      setting: listSetting,
      total: total,
      message: req.flash("message"),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al buscar.",
    });
  }
};

/***** CONTROLLERS *****/
exports.one = async function (req, res) {
  try {
    const { id } = req.params;
    const setting = await Setting.findById(id);
    if (!setting) {
      req.flash("message", {
        icon: "error",
        msg: "La configuración no existe",
      });
      res.redirect("/setting");
    }

    res.render("admin/setting/new", {
      title: "Configuración - LoxaLibre",
      setting,
      message: req.flash("message"),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al buscar.",
    });
    res.redirect("/setting");
  }
};

exports.save = async function (req, res) {
  try {
    console.log("\n\nBODY", JSON.parse(req.body.data));
    console.log("\n\nFILES", req.files);

    let data = JSON.parse(req.body.data);

    const newSetting = new Setting(data);

    if (req.files && req.files.image) {
      let path_to_folder = __dirname.replace(
        "controllers",
        `public/images/setting/${newSetting._id}/`
      );

      fs.mkdirSync(path_to_folder, { recursive: true }, (err) => {
        if (err) throw err;
      });

      var writeStream = fs.createWriteStream(path_to_folder + ".gitignore");
      writeStream.write("*");
      writeStream.close();
      let imageFile = req.files.image;

      imageFile.mv(
        path_to_folder + newSetting._id + "." + types_image[imageFile.mimetype],
        (errorofs) => {
          errorofs ? console.error(errorofs) : console.log("foto subida");
        }
      );
      newSetting.image =
      `/images/setting/${newSetting._id}/` +
      newSetting._id +
      "." +
      types_image[imageFile.mimetype];
    }

    if (data.icon) {
      for (const itemArray of data.icon) {
        console.log("itemArray", itemArray);
        let path_to_folder = __dirname.replace(
          "controllers",
          `public/images/setting/${newSetting._id}/icon/`
        );

        fs.mkdirSync(path_to_folder, { recursive: true }, (err) => {
          if (err) throw err;
        });

        var writeStream = fs.createWriteStream(path_to_folder + ".gitignore");
        writeStream.write("*");
        writeStream.close();

        let iconPhoto = req.files[`image-${itemArray.id}`];

        iconPhoto.mv(
          path_to_folder +
            newSetting.icon[itemArray.id]._id +
            "." +
            types_image[iconPhoto.mimetype],
          (errorofs) => {
            errorofs ? console.error(errorofs) : console.log("foto subida");
          }
        );

        newSetting.icon[itemArray.id].image =
          `/images/icon/${newSetting._id}/icon/` +
          newSetting.icon[itemArray.id]._id +
          "." +
          types_image[iconPhoto.mimetype];
      }
    }

    await newSetting.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha creado la configuración: " +
        newSetting.text,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: "Creado con éxito.",
    });
    res.json({ redirect: `/setting` });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al guardar.",
    });
    res.json({ redirect: `/setting/new` });
  }
};

exports.update = async function (req, res) {
  try {
    let data = JSON.parse(req.body.data);

    let setting = await Setting.findById(req.params.id);
    setting.text = data.text;
    setting.text_position = data.text_position;
    setting.image_position = data.image_position;
    setting.array = data.array;
    setting.array_position = data.array_position;

    if (req.files && req.files.icon) {
      let path_to_folder = __dirname.replace(
        "controllers",
        `public/images/setting/${req.params.id}/`
      );

      let iconFile = req.files.icon;

      iconFile.mv(
        path_to_folder + req.params.id + "." + types_image[iconFile.mimetype],
        (errorofs) => {
          errorofs ? console.error(errorofs) : console.log("foto subida");
        }
      );
    }

    if (data.array) {
      for (const itemArray of data.array) {
        let path_to_folder = __dirname.replace(
          "controllers",
          `public/images/setting/${setting._id}/array/`
        );

        let arrayPhoto = req.files[`icon-${itemArray.id}`];

        if (arrayPhoto) {
          let arrayId = setting.array[itemArray.id]
            ? setting.array[itemArray.id]._id
            : mongoose.Types.ObjectId();

          arrayPhoto.mv(
            path_to_folder + arrayId + "." + types_image[arrayPhoto.mimetype],
            (errorofs) => {
              errorofs ? console.error(errorofs) : console.log("foto subida");
            }
          );

          setting.array[itemArray.id].icon =
            `/images/setting/${setting._id}/array/` +
            arrayId +
            "." +
            types_image[arrayPhoto.mimetype];

          setting.array[itemArray.id]._id = arrayId;
        }
      }
    }

    await setting.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha modificado la configuración: " +
        setting.text,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: "Modificado con éxito",
    });
    res.json({ redirect: `/setting` });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al modificar.",
    });
    res.json({ redirect: `/setting/update/${req.params.id}` });
  }
};

exports.toggleStatus = async function (req, res) {
  try {
    let setting = await Setting.findById(req.params.id);
    setting.status = !setting.status;
    await setting.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha cambiado el estado de la configuración: " +
        setting.title,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: setting.status ? "Activado con éxito" : "Suspendido con éxito.",
    });
    res.redirect(`/setting`);
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al suspender.",
    });
    res.redirect(`/setting`);
  }
};

exports.delete = async function (req, res) {
  try {
    let setting = await Setting.findByIdAndDelete(req.params.id);
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha eliminado la configuración: " +
        setting.text,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: "Eliminado con éxito",
    });
    res.redirect("/setting");
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al eliminar.",
    });
    res.redirect("/setting");
  }
};
