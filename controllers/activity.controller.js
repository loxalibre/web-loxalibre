"use strict";

const Activity = require("../models/activity.model");
const Binnacle = require("../models/binnacle.model");
const Tag = require("../models/tag.model");

const fs = require("fs");
const mongoose = require("mongoose");
const { types_image } = require("./utils");
/***** VIEWS *****/

/* test */
exports.test = function (req, res) {
  res.send("Greetings from the Activity controller!");
};
/*list*/
exports.list = async function (req, res) {
  try {
    // queries
    const { page, rowsPerPage, orderBy, direction } = req.query;
    const res_per_page = parseFloat(rowsPerPage) || 10;
    const current_page = parseFloat(page) || 1;

    const listActivity = await Activity.find()
      .sort({ [orderBy || "createdAt"]: direction || "desc" })
      .skip(res_per_page * current_page - res_per_page)
      .limit(res_per_page);
    const total = await Activity.countDocuments({
      type: req.body.type,
    });
    res.render("admin/activity/list", {
      title: "Actividades - LoxaLibre",
      listActivity: listActivity,
      total: total,
      message: req.flash("message"),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al buscar.",
    });
  }
};

/***** CONTROLLERS *****/

exports.one = async function (req, res) {
  try {
    const { id } = req.params;
    const activity = await Activity.findById(id).populate("tags");
    if (!activity) {
      req.flash("message", {
        icon: "error",
        msg: "La actividad no existe",
      });
      res.redirect("/activity");
    }

    const tags = await Tag.find();

    res.render("admin/activity/new", {
      title: "Actividad - LoxaLibre",
      activity,
      tags,
      message: req.flash("message"),
    });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al buscar.",
    });
    res.redirect("/activity");
  }
};

exports.save = async function (req, res) {
  try {
    console.log("\n\nBODY", JSON.parse(req.body.data));
    console.log("\n\nFILES", req.files);

    let data = JSON.parse(req.body.data);
    data.author = req.user._id;

    let tags = data.tags.split(",");
    var tags_id = [];
    if (Array.isArray(tags) && tags.length > 0) {
      for (var i = 0; i < tags.length; i++) {
        let name = tags[i].trim().toUpperCase();
        const tag_find = await Tag.findOne({ name: name });
        if (tag_find) {
          tags_id.push(tag_find._id);
        } else {
          let new_tag = new Tag({ name: name });
          await new_tag.save();
          tags_id.push(new_tag._id);
        }
      }
    }
    data.tags = tags_id;

    const newActivity = new Activity(data);

    if (req.files && req.files.art) {
      let path_to_folder = __dirname.replace(
        "controllers",
        `public/images/activity/${newActivity._id}/`
      );

      fs.mkdirSync(path_to_folder, { recursive: true }, (err) => {
        if (err) throw err;
      });

      var writeStream = fs.createWriteStream(path_to_folder + ".gitignore");
      writeStream.write("*");
      writeStream.close();
      let artFile = req.files.art;

      artFile.mv(
        path_to_folder + newActivity._id + "." + types_image[artFile.mimetype],
        (errorofs) => {
          errorofs ? console.error(errorofs) : console.log("foto subida");
        }
      );

      newActivity.art =
        `/images/activity/${newActivity._id}/` +
        newActivity._id +
        "." +
        types_image[artFile.mimetype];
    }

    if (data.guest) {
      for (const itemGuest of data.guest) {
        console.log("itemGuest", itemGuest);
        let path_to_folder = __dirname.replace(
          "controllers",
          `public/images/activity/${newActivity._id}/guest/`
        );

        fs.mkdirSync(path_to_folder, { recursive: true }, (err) => {
          if (err) throw err;
        });

        var writeStream = fs.createWriteStream(path_to_folder + ".gitignore");
        writeStream.write("*");
        writeStream.close();

        let guestPhoto = req.files[`guest_photo-${itemGuest.id}`];

        guestPhoto.mv(
          path_to_folder +
            newActivity.guest[itemGuest.id]._id +
            "." +
            types_image[guestPhoto.mimetype],
          (errorofs) => {
            errorofs ? console.error(errorofs) : console.log("foto subida");
          }
        );

        newActivity.guest[itemGuest.id].guest_photo =
          `/images/activity/${newActivity._id}/guest/` +
          newActivity.guest[itemGuest.id]._id +
          "." +
          types_image[guestPhoto.mimetype];
      }
    }

    await newActivity.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha creado la actividad: " +
        newActivity.title,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", { icon: "success", msg: "Creado con éxito." });
    res.json({ redirect: "/activity" });
  } catch (error) {
    console.error("\n\n Error", error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al guardar.",
    });
    res.json({ redirect: "/activity/new" });
  }
};

exports.update = async function (req, res) {
  try {
    console.log("\n\nBODY UPDATE", JSON.parse(req.body.data));
    console.log("\n\nFILES UPDATE", req.files);

    let data = JSON.parse(req.body.data);

    let activity = await Activity.findById(req.params.id);
    activity.title = data.title;
    activity.lead = data.lead;
    activity.description = data.description;
    activity.date_start = data.date_start;
    activity.date_end = data.date_end;
    activity.means_places = data.means_places;
    activity.type = data.type;
    activity.guest = data.guest;

    let tags = data.tags.split(",");
    var tags_id = [];
    if (Array.isArray(tags) && tags.length > 0) {
      for (var i = 0; i < tags.length; i++) {
        let name = tags[i].trim().toUpperCase();
        const tag_find = await Tag.findOne({ name: name });
        if (tag_find) {
          tags_id.push(tag_find._id);
        } else {
          let new_tag = new Tag({ name: name });
          await new_tag.save();
          tags_id.push(new_tag._id);
        }
      }
    }
    activity.tags = tags_id;

    if (req.files && req.files.art) {
      let path_to_folder = __dirname.replace(
        "controllers",
        `public/images/activity/${req.params.id}/`
      );

      let artFile = req.files.art;

      artFile.mv(
        path_to_folder + req.params.id + "." + types_image[artFile.mimetype],
        (errorofs) => {
          errorofs ? console.error(errorofs) : console.log("foto subida");
        }
      );
    }

    if (data.guest) {
      for (const itemGuest of data.guest) {
        let path_to_folder = __dirname.replace(
          "controllers",
          `public/images/activity/${activity._id}/guest/`
        );

        let guestPhoto = req.files[`guest_photo-${itemGuest.id}`];

        if (guestPhoto) {
          let guestId = activity.guest[itemGuest.id]
            ? activity.guest[itemGuest.id]._id
            : mongoose.Types.ObjectId();

          guestPhoto.mv(
            path_to_folder + guestId + "." + types_image[guestPhoto.mimetype],
            (errorofs) => {
              errorofs ? console.error(errorofs) : console.log("foto subida");
            }
          );

          activity.guest[itemGuest.id].guest_photo =
            `/images/activity/${activity._id}/guest/` +
            guestId +
            "." +
            types_image[guestPhoto.mimetype];

          activity.guest[itemGuest.id]._id = guestId;
        }
      }
    }

    await activity.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha modificado la actividad: " +
        activity.title,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", { icon: "success", msg: "Modificado con éxito." });
    res.json({ redirect: `/activity` });
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al modificar.",
    });
    res.json({ redirect: `/activity/update/${req.params.id}` });
  }
};

exports.toggleStatus = async function (req, res) {
  try {
    let activity = await Activity.findById(req.params.id);
    activity.status = !activity.status;
    await activity.save();
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha cambiado el estado de la actividad: " +
        activity.title,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: activity.status ? "Activado con éxito" : "Suspendido con éxito.",
    });
    res.redirect(`/activity`);
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al suspender.",
    });
    res.redirect(`/activity`);
  }
};

exports.delete = async function (req, res) {
  try {
    let activity = await Activity.findByIdAndDelete(req.params.id);
    const binnacle = new Binnacle({
      date_action: new Date(),
      description:
        "El " +
        req.user.rol +
        ": " +
        req.user.name +
        " " +
        req.user.last_name +
        " ha eliminado la actividad: " +
        activity.title,
      author: data.author,
    });
    await binnacle.save();
    req.flash("message", {
      icon: "success",
      msg: "Eliminado con éxito",
    });
    res.redirect(`/activity`);
  } catch (error) {
    console.error(error);
    req.flash("message", {
      icon: "error",
      msg: "Ocurrió un error al eliminar.",
    });
    res.redirect(`/activity`);
  }
};
