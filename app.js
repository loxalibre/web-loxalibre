var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var passport = require("passport");
var session = require("express-session");
var bodyParser = require("body-parser");
var flash = require("connect-flash");
var fs = require("fs");
var fileUpload = require("express-fileupload");
var mongoose = require("mongoose");

let dev_db_url = "mongodb://localhost/loxalibre";
let mongoDB = process.env.MONGODB_URI || dev_db_url;
mongoose.connect(
  mongoDB,
  { useCreateIndex: true, useNewUrlParser: true, useUnifiedTopology: true },
  function (err) {
    if (err) {
      console.error(err);
    } else {
      console.log("Successfully connected to MongoDB");
    }
  }
);
mongoose.Promise = global.Promise;
let db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error:"));

global.THIS_DOMAIN = "http://localhost:3000/";

var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var aboutRouter = require("./routes/about.routes");
var articleRouter = require("./routes/article.routes");
var projectRouter = require("./routes/project.routes");
var activityRouter = require("./routes/activity.routes");
var eventRouter = require("./routes/event.routes");
var registerRouter = require("./routes/register.routes");
var settingRouter = require("./routes/setting.routes");
var tagRouter = require("./routes/tag.routes");

var app = express();

app.use(bodyParser.json({ limit: "50mb" }));
app.use(bodyParser.urlencoded({ limit: "50mb", extended: false }));

require("./config/passport/passport.js")(passport);

//For Passport
app.use(
  session({
    secret: "qwertyuioopasdfghjklñzxcvbnm7410852963",
    resave: true,
    saveUninitialized: true,
  })
); //session secret
app.use(passport.initialize());
app.use(passport.session()); // Persistent login session
app.use(function (req, res, next) {
  res.locals.user = req.user;
  next();
});

app.use(fileUpload());

//conect-flash
app.use(flash());

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/about", aboutRouter);
app.use("/article", articleRouter);
app.use("/project", projectRouter);
app.use("/activity", activityRouter);
app.use("/event", eventRouter);
app.use("/register", registerRouter);
app.use("/setting", settingRouter);
app.use("/tag", tagRouter);
app.use("/users", usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
