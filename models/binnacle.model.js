require('./user.model');

var mongoose = require('mongoose');

var User = mongoose.model('User');
var Schema = mongoose.Schema;

var schema = new Schema ({
    date_action: {
      type: Date,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
});

module.exports = mongoose.model('Binnacle', schema);
