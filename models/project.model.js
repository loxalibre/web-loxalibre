require('./tag.model');
require('./user.model');

var mongoose = require('mongoose');

var Tag = mongoose.model('Tag');
var User = mongoose.model('User');
var Schema = mongoose.Schema;

var schema = new Schema ({
  name: {
    type: String,
    required: true
  },
  lead: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true,
  },
  description_json: {
    type: String,
    required: true,
  },
  art: {
    type: String,
    required: true
  },
  date_start: {
    type: Date,
    required: true
  },
  duration: {
    type: String,
    required: true
  },
  tags: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "Tag"
  }],
  team: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  }],
  pm: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User"
  },
  status: {
    type: Boolean,
    default: true
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    required: true
  }
});

module.exports = mongoose.model('Project', schema);
