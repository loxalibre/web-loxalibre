var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var schema = new Schema(
  {
    name: {
      type: String,
      required: true,
    },
    last_name: {
      type: String,
      required: true,
    },
    mail: {
      type: String,
      required: true,
    },
    occupation: {
      type: String,
    },
    place_occupation: {
      type: String,
    },
    age: {
      type: String,
    },
    origin: [
      {
        country: {
          type: String,
        },
        city: {
          type: String,
        },
      },
    ],
    activity: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Activity",
      },
    ],
    event: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Event",
      },
    ],
    status: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Register", schema);
