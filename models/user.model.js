var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var schema = new Schema({
  name: {
    type: String,
    required: true
  },
  last_name: {
    type: String,
    required: true
  },
  biography: {
    type: String
  },
  state: {
    type: String
  },
  rol: {
    type: String,
    default: "Member"
  },
  password: {
    type: String,
    required: true
  },
  photo: {
    type: String,
    required: true
  },
  mail: {
    type: String,
    required: true,
    unique: true
  },
  show_mail: {
    type: Boolean,
    default: false
  },
  phones: [{
    type: String
  }],
  show_phones: {
    type: Boolean,
    default: false
  },
  country: {
    type: String,
  },
  show_country: {
    type: Boolean,
    default: false
  },
  province: {
    type: String,
  },
  show_province: {
    type: Boolean,
    default: false
  },
  city: {
    type: String,
  },
  show_city: {
    type: Boolean,
    default: false
  },
  address: {
    type: String,
  },
  show_address: {
    type: Boolean,
    default: false
  },
  //Repetir estas en todos los modelos
  status: {
    type: Boolean,
    default: true
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    required: true
  }
});

module.exports = mongoose.model('User', schema);
