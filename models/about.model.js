require('./user.model');

var mongoose = require('mongoose');

var User = mongoose.model('User');
var Schema = mongoose.Schema;

var schema = new Schema ({
  title: {
    type: String,
    required: true
  },
  introduction: {
    type: String,
    required: true
  },
  image_slider: [{
    img: {
      type: String
    },
    isVisible: {
      type: Boolean,
      default: true
    }
  }],
  points: [{
    title: {type: String},
    lead: {type: String},
    description: {type: String},
    description_json: {type: String},
    date: {type: Date, default: Date.now},
    isVisible: {type: Boolean, default: true},
    date_updated: {type: Date},
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    },
  }],
  status: {
    type: Boolean,
    default: true
  },
  created_at: {
    type: Date,
    default: Date.now
  },
  updated_at: {
    type: Date,
    required: true
  }
});

module.exports = mongoose.model('About', schema);
