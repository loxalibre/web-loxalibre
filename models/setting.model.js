var mongoose = require("mongoose");

var Schema = mongoose.Schema;
var schema = new Schema(
  {
    location: {
      type: String,
      required: true
    },
    text: {
      type: String,
    },
    text_position: {
      type: String,
    },
    image: {
      type: String,
    },
    image_position: {
      type: String,
    },
    array: [
      {
        title: { type: String },
        url: { type: String },
        icon: { type: String },
      },
    ],
    array_position: {
      type: String,
    },
    status: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Setting", schema);
