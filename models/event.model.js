var mongoose = require("mongoose");

var Schema = mongoose.Schema;
var schema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    lead: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    art: {
      type: String,
      // required: true,
    },
    registration_start: {
      type: Date,
      required: true,
    },
    registration_end: {
      type: Date,
      required: true,
    },
    means_place: {
      type: String,
      required: true,
    },
    tags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tag",
      },
    ],
    organizer: [
      {
        name: {
          type: String,
          required: true,
        },
        logo: {
          type: String,
          // required: true,
        },
      },
    ],
    responsible: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    status: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Event", schema);
