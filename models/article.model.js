require("./tag.model");
require("./user.model");

var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var schema = new Schema({
  title: {
    type: String,
    required: true,
  },
  lead: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  description_json: {
    type: String,
    required: true,
  },
  art: {
    type: String,
    required: true,
  },
  tags: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Tag",
    },
  ],
  author: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
  },
  status: {
    type: Boolean,
    default: true
  },
  created_at: {
    type: Date,
    default: Date.now,
  },
  updated_at: {
    type: Date,
    required: true,
  },
});

module.exports = mongoose.model("Article", schema);
