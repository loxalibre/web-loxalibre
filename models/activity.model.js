var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var schema = new Schema(
  {
    title: {
      type: String,
      required: true,
    },
    lead: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    tags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tag",
      },
    ],
    guest: [
      {
        name: {
          type: String,
          required: true,
        },
        contact: {
          type: String,
        },
        minibiography: {
          type: String,
          required: true,
        },
        guest_photo: {
          type: String,
          // required: true,
        },
      },
    ],
    art: {
      type: String,
      // required: true,
    },
    date_start: {
      type: Date,
      required: true,
    },
    date_end: {
      type: Date,
      required: true,
    },
    means_places: {
      type: String,
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    status: {
      type: Boolean,
      default: true,
    },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Activity", schema);
